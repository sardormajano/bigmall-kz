module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '138.68.153.61',
      username: 'root',
      // pem: './path/to/pem'
      password: '4thebest'
      // or neither for authenticate from ssh-agent
    }
  },

  meteor: {
      name: 'bigmall',
      path: '..',
      dockerImage: 'abernix/meteord:base',
      servers: {
        one: {}
      },
      buildOptions: {
        serverOnly: true,
      },
      env: {
        ROOT_URL: 'http://localhost:3000/',
        MONGO_URL: 'http://localhost:3001/'
      },
      //dockerImage: 'kadirahq/meteord'
      deployCheckWaitTime: 120,
      enableUploadProgressBar: true
  },

  mongo: {
    port: 27017,
    version: '3.4.1',
    servers: {
      one: {}
    }
  }
};
