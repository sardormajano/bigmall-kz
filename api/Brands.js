import {Mongo} from 'meteor/mongo';

export const BrandsCollection = new Mongo.Collection('brands');

if(Meteor.isServer) {
  Meteor.publish('Brands', () => {
    return BrandsCollection.find();
  });

  Meteor.methods({
    'brands.add'(data) {
      data.createdBy = Meteor.userId;
      const createdAt = new Date();
      data.createdAt = Date.parse(createdAt);

      BrandsCollection.insert(data);
    },
    'brands.remove'(_id) {
      BrandsCollection.remove({_id})
    },
    'brands.edit'(_id, data) {
      data.updatedBy = Meteor.userId;
      const updatedAt = new Date();
      data.updatedAt = Date.parse(updatedAt);

      BrandsCollection.update({_id}, {$set: data});
    }
  });
}
