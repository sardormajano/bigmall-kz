import {Mongo} from 'meteor/mongo';

export const CategoriesCollection = new Mongo.Collection('categories');

if(Meteor.isServer) {
  Meteor.publish('Categories', () => {
    return CategoriesCollection.find();
  });

  Meteor.methods({
    'categories.add'(data) {
      data.createdBy = Meteor.userId;
      const createdAt = new Date();
      data.createdAt = Date.parse(createdAt);

      CategoriesCollection.insert(data);
    },
    'categories.remove'(_id) {
      CategoriesCollection.remove({_id})
    },
    'categories.edit'(_id, data) {
      data.updatedBy = Meteor.userId;
      const updatedAt = new Date();
      data.updatedAt = Date.parse(updatedAt);

      CategoriesCollection.update({_id}, {$set: data});
    }
  });
}
