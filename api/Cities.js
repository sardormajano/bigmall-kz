import {Mongo} from 'meteor/mongo';

export const CitiesCollection = new Mongo.Collection('cities');

if(Meteor.isServer) {
  Meteor.publish('Cities', () => {
    return CitiesCollection.find();
  });
}
