import {Mongo} from 'meteor/mongo';

export const ExtraFeaturesCollection = new Mongo.Collection('extraFeatures');

if(Meteor.isServer) {
  Meteor.publish('ExtraFeatures', () => {
    return ExtraFeaturesCollection.find();
  });

  Meteor.methods({
    'extraFeatures.add'(data) {
      data.createdBy = Meteor.userId;
      const createdAt = new Date();
      data.createdAt = Date.parse(createdAt);

      ExtraFeaturesCollection.insert(data);
    },
    'extraFeatures.remove'(_id) {
      ExtraFeaturesCollection.remove({_id})
    },
    'extraFeatures.edit'(_id, data) {
      data.updatedBy = Meteor.userId;
      const updatedAt = new Date();
      data.updatedAt = Date.parse(updatedAt);

      ExtraFeaturesCollection.update({_id}, {$set: data});
    }
  });
}
