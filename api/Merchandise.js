import {Mongo} from 'meteor/mongo';

export const MerchandiseCollection = new Mongo.Collection('merchandise');

export const merchandisePhotos = new FileCollection('merchandisePhotos', {
  resumable: true,   // Enable built-in resumable.js upload support
  http: [
    {
      method: 'get',
      path: '/:md5',  // this will be at route "/gridfs/merchandisePhotos/:md5"
      lookup: function (params, query) {  // uses express style url params
        return { md5: params.md5 };       // a query mapping url to myFiles
      }
    }
  ]
});

if(Meteor.isServer) {
  Meteor.publish('MerchandisePhotos', (clientUserId) => {
      return merchandisePhotos.find({});
    }
  );

  merchandisePhotos.allow({
    // The creator of a file owns it. UserId may be null.
    insert: function (userId, file) {
      return true;
    },
    // Only owners can remove a file
    remove: function (userId, file) {
      return true;
    },
    // Only owners can retrieve a file via HTTP GET
    read: function (userId, file) {
      return true;
    },
    // This rule secures the HTTP REST interfaces' PUT/POST
    // Necessary to support Resumable.js
    write: function (userId, file, fields) {
      // Only owners can upload file data
      return true;
    }
  });

  Meteor.publish('Merchandise', function() {
      return MerchandiseCollection.find({
        createdBy: this.userId,
        deleted: {$ne: true}
      });
  });

  Meteor.publish('StoreMerchandise', function(store) {
      return MerchandiseCollection.find({
        createdBy: this.userId,
        store,
        deleted: {$ne: true}
      });
  });

  Meteor.publish('AllMerchandise', function() {
      return MerchandiseCollection.find({
        deleted: {$ne: true}
      });
  });

  Meteor.methods({
    'merchandise.add'(data) {
      data.createdBy = Meteor.userId();
      const createdAt = new Date();
      data.createdAt = Date.parse(createdAt);

      MerchandiseCollection.insert(data);
    },
    'merchandise.remove'(_id) {
      MerchandiseCollection.update({_id}, {$set: {deleted: true}});
    },
    'merchandise.edit'(_id, data) {
      data.updatedBy = Meteor.userId();
      const updatedAt = new Date();
      data.updatedAt = Date.parse(updatedAt);

      MerchandiseCollection.update({_id}, {$set: data});
    }
  });
}
