import {Mongo} from 'meteor/mongo';

export const RecommendedCategoriesCollection = new Mongo.Collection('recommendedCategories');

export const recommendedCategoriesPhotos = new FileCollection('recommendedCategoriesPhotos', {
  resumable: true,   // Enable built-in resumable.js upload support
  http: [
    {
      method: 'get',
      path: '/:md5',  // this will be at route "/gridfs/recommendedCategoriesPhotos/:md5"
      lookup: function (params, query) {  // uses express style url params
        return { md5: params.md5 };       // a query mapping url to myFiles
      }
    }
  ]
});

if(Meteor.isServer) {
  Meteor.publish('RecommendedCategoriesPhotos', (clientUserId) => {
      return recommendedCategoriesPhotos.find({});
    }
  );

  recommendedCategoriesPhotos.allow({
    // The creator of a file owns it. UserId may be null.
    insert: function (userId, file) {
      return true;
    },
    // Only owners can remove a file
    remove: function (userId, file) {
      return true;
    },
    // Only owners can retrieve a file via HTTP GET
    read: function (userId, file) {
      return true;
    },
    // This rule secures the HTTP REST interfaces' PUT/POST
    // Necessary to support Resumable.js
    write: function (userId, file, fields) {
      // Only owners can upload file data
      return true;
    }
  });

  Meteor.publish('RecommendedCategories', () => {
    return RecommendedCategoriesCollection.find({deleted: {$ne: true}});
  });

  Meteor.methods({
    'recommendedCategories.add'(data) {
      data.createdBy = Meteor.userId;
      const createdAt = new Date();
      data.createdAt = Date.parse(createdAt);

      RecommendedCategoriesCollection.insert(data);
    },
    'recommendedCategories.remove'(_id) {
      RecommendedCategoriesCollection.update({_id}, {$set: {deleted: true}});
    },
    'recommendedCategories.edit'(_id, data) {
      data.updatedBy = Meteor.userId;
      const updatedAt = new Date();
      data.updatedAt = Date.parse(updatedAt);

      RecommendedCategoriesCollection.update({_id}, {$set: data});
    }
  });
}
