import {Mongo} from 'meteor/mongo';

export const RecommendedCollectionsCollection = new Mongo.Collection('recommendedCollections');

export const recommendedCollectionsPhotos = new FileCollection('recommendedCollectionsPhotos', {
  resumable: true,   // Enable built-in resumable.js upload support
  http: [
    {
      method: 'get',
      path: '/:md5',  // this will be at route "/gridfs/recommendedCollectionsPhotos/:md5"
      lookup: function (params, query) {  // uses express style url params
        return { md5: params.md5 };       // a query mapping url to myFiles
      }
    }
  ]
});

if(Meteor.isServer) {
  Meteor.publish('RecommendedCollectionsPhotos', (clientUserId) => {
      return recommendedCollectionsPhotos.find({});
    }
  );

  recommendedCollectionsPhotos.allow({
    // The creator of a file owns it. UserId may be null.
    insert: function (userId, file) {
      return true;
    },
    // Only owners can remove a file
    remove: function (userId, file) {
      return true;
    },
    // Only owners can retrieve a file via HTTP GET
    read: function (userId, file) {
      return true;
    },
    // This rule secures the HTTP REST interfaces' PUT/POST
    // Necessary to support Resumable.js
    write: function (userId, file, fields) {
      // Only owners can upload file data
      return true;
    }
  });

  Meteor.publish('RecommendedCollections', () => {
    return RecommendedCollectionsCollection.find({deleted: {$ne: true}});
  });

  Meteor.methods({
    'recommendedCollections.add'(data) {
      data.createdBy = Meteor.userId;
      const createdAt = new Date();
      data.createdAt = Date.parse(createdAt);

      RecommendedCollectionsCollection.insert(data);
    },
    'recommendedCollections.remove'(_id) {
      RecommendedCollectionsCollection.update({_id}, {$set: {deleted: true}});
    },
    'recommendedCollections.edit'(_id, data) {
      data.updatedBy = Meteor.userId;
      const updatedAt = new Date();
      data.updatedAt = Date.parse(updatedAt);

      RecommendedCollectionsCollection.update({_id}, {$set: data});
    }
  });
}
