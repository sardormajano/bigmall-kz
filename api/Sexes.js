import {Mongo} from 'meteor/mongo';

export const SexesCollection = new Mongo.Collection('sexes');

if(Meteor.isServer) {
  Meteor.publish('Sexes', () => {
    return SexesCollection.find();
  });
}
