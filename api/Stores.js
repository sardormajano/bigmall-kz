import {Mongo} from 'meteor/mongo';

import {UsersCollection} from './Users';

export const StoresCollection = new Mongo.Collection('stores');

export const storeLogos = new FileCollection('storeLogos', {
  resumable: true,   // Enable built-in resumable.js upload support
  http: [
    {
      method: 'get',
      path: '/:md5',  // this will be at route "/gridfs/storeLogos/:md5"
      lookup: function (params, query) {  // uses express style url params
        return { md5: params.md5 };       // a query mapping url to myFiles
      }
    }
  ]
});

if(Meteor.isServer) {
  Meteor.publish('StroreLogos', (clientUserId) => {
      return storeLogos.find({});
    }
  );

  storeLogos.allow({
    // The creator of a file owns it. UserId may be null.
    insert: function (userId, file) {
      return true;
    },
    // Only owners can remove a file
    remove: function (userId, file) {
      return true;
    },
    // Only owners can retrieve a file via HTTP GET
    read: function (userId, file) {
      return true;
    },
    // This rule secures the HTTP REST interfaces' PUT/POST
    // Necessary to support Resumable.js
    write: function (userId, file, fields) {
      // Only owners can upload file data
      return true;
    }
  });

  Meteor.publish('Stores', () => {
    return StoresCollection.find();
  });

  Meteor.publish('Subscriptions', function() {
    const theUser = UsersCollection.findOne({_id: this.userId}),
          userSubscriptions = theUser.profile.subscriptions || [];

    return StoresCollection.find({_id: {$in: userSubscriptions}});
  });

  Meteor.methods({
    'stores.add'(data) {
      data.createdBy = Meteor.userId;
      const createdAt = new Date();
      data.createdAt = Date.parse(createdAt);

      StoresCollection.insert(data);
    },
    'stores.remove'(_id) {
      StoresCollection.remove({_id})
    },
    'stores.edit'(_id, data) {
      data.updatedBy = Meteor.userId;
      const updatedAt = new Date();
      data.updatedAt = Date.parse(updatedAt);

      StoresCollection.update({_id}, {$set: data});
    }
  });
}
