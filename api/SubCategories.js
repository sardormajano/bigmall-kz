import {Mongo} from 'meteor/mongo';

export const SubCategoriesCollection = new Mongo.Collection('subCategories');

if(Meteor.isServer) {
  Meteor.publish('SubCategories', () => {
    return SubCategoriesCollection.find();
  });

  Meteor.methods({
    'subCategories.add'(data) {
      data.createdBy = Meteor.userId;
      const createdAt = new Date();
      data.createdAt = Date.parse(createdAt);

      SubCategoriesCollection.insert(data);
    },
    'subCategories.remove'(_id) {
      SubCategoriesCollection.remove({_id})
    },
    'subCategories.edit'(_id, data) {
      data.updatedBy = Meteor.userId;
      const updatedAt = new Date();
      data.updatedAt = Date.parse(updatedAt);

      SubCategoriesCollection.update({_id}, {$set: data});
    }
  });
}
