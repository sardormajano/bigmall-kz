export const UsersCollection = Meteor.users;

export const userPhotos = new FileCollection('userPhotos', {
  resumable: true,   // Enable built-in resumable.js upload support
  http: [
    {
      method: 'get',
      path: '/:md5',  // this will be at route "/gridfs/userPhotos/:md5"
      lookup: function (params, query) {  // uses express style url params
        return { md5: params.md5 };       // a query mapping url to myFiles
      }
    }
  ]
});

if(Meteor.isServer) {
  Meteor.publish('photos', (clientUserId) => {
      return userPhotos.find({});
    }
  );

  userPhotos.allow({
    // The creator of a file owns it. UserId may be null.
    insert: function (userId, file) {
      return true;
    },
    // Only owners can remove a file
    remove: function (userId, file) {
      return true;
    },
    // Only owners can retrieve a file via HTTP GET
    read: function (userId, file) {
      return true;
    },
    // This rule secures the HTTP REST interfaces' PUT/POST
    // Necessary to support Resumable.js
    write: function (userId, file, fields) {
      // Only owners can upload file data
      return true;
    }
  });

  Meteor.publish('UsersList', function() {
    return UsersCollection.find({'profile.deleted': {$ne: true}});
  });

  Meteor.publish('CurrentUser', function() {
    return UsersCollection.find({_id: this.userId});
  });

  Meteor.methods({
    'user.create'(userOptions, role) {
      if(UsersCollection.find({username: userOptions.username}).count()) {
        const currentUser = UsersCollection.find({username: userOptions.username}).fetch()[0];
        UsersCollection.update({_id: currentUser._id}, {$set: {'profile.deleted': false}});
        Accounts.setPassword(currentUser._id, userOptions.newPassword);
      }
      else {
        const userId = Accounts.createUser(userOptions);
        Roles.addUsersToRoles(userId, role);
      }
    },
    'user.update'(data) { //for updating current user
      if(data)
        Meteor.users.update( Meteor.userId(), {$set: data});
    },
    'admin.update'(_id, data, password) { //for updating other user
      Meteor.users.update( {_id}, {$set: data});
      if(password !== '')
        Accounts.setPassword(_id, password);
    },
    'user.remove'(_id, data) {
        Meteor.users.update( {_id}, {$set: data});
    },
    'user.changePassword'(oldPassword, newPassword) {
      Accounts.changePassword(oldPassword, newPassword);
    },
    'user.subscribe'(store) {
      if(!Meteor.user().profile.subscriptions || Meteor.user().profile.subscriptions.indexOf(store) === -1)
        Meteor.users.update( Meteor.userId(), {$push: {'profile.subscriptions': store}});
    },
    'user.unsubscribe'(store) {
      Meteor.users.update( Meteor.userId(), {$pull: {'profile.subscriptions': store}});
    },
    'user.wishlist.add'(merchandiseId) {
      if(!Meteor.user().profile.wishlist || Meteor.user().profile.wishlist.indexOf(merchandiseId) === -1)
        Meteor.users.update( Meteor.userId(), {$push: {'profile.wishlist': merchandiseId}});
    },
    'user.deleteFromWishlist'(data) {
      Meteor.users.update( Meteor.userId(), {$pull: {'profile.wishlist': data.merchandiseId}});
    },
    'user.cart.add'(merchandiseId) {
      if(!Meteor.user().profile.cart || Meteor.user().profile.cart.indexOf(merchandiseId) === -1)
        Meteor.users.update( Meteor.userId(), {$push: {'profile.cart': merchandiseId}});
    },
    'user.deleteFromCart'(data) {
      Meteor.users.update( Meteor.userId(), {$pull: {'profile.cart': data.merchandiseId}});
    },
    'user.clearCart'(data) {
      Meteor.users.update( Meteor.userId(), {$set: {'profile.cart': []}});
    },
    'user.changePhoto'(photoId) {
      console.log('here');
      Meteor.users.update( Meteor.userId(), {$set: {'profile.photo': photoId}})
    }
  });
}
