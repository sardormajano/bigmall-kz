import React, {Component} from 'react';
import {qs} from '../lib/coreLib';

export default class App extends Component {
  componentDidMount() {
    qs('body').setAttribute('class', 'body-wrapper');
  }

  render() {
    return (
      <div className='main-wrapper'>
        {this.props.children}
      </div>
    )
  }
};
