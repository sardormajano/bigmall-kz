import React, {Component} from 'react';

import Header from '../stateless/Header';
import PageHeader from '../stateless/PageHeader';
import Footer from '../stateless/Footer';
import Copyright from '../stateless/Copyright';
import Navigation from '../stateless/Navigation';

import AdministratorsTab from './tabs/AdministratorsTab';
import CategoriesTab from './tabs/CategoriesTab';
import SubCategoriesTab from './tabs/SubCategoriesTab';
import BrandsTab from './tabs/BrandsTab';
import ExtraFeaturesTab from './tabs/ExtraFeaturesTab';

import Pikaday from '../stateless/Pikaday';
import Select2 from '../stateless/Select2';

import SuperAdminHelmet from '../../helmets/SuperAdminHelmet';

export default class SuperAdmin extends Component {
  componentDidMount() {
    const {userPhotos} = this.props;

    userPhotos.resumable.assignBrowse($("#input-photo"));

    userPhotos.resumable.on('fileAdded', (file) => {
      Meteor.call('user.changePhoto', (file.uniqueIdentifier), (err) => {
        if(err) {
          Bert.alert( err.reason, 'danger', 'growl-top-right' );
        }
        else {
          Bert.alert( 'Фотография изменена!', 'success', 'growl-top-right' );
        }
      });

      userPhotos.insert({
        _id: file.uniqueIdentifier,  // This is the ID resumable will use
        filename: file.fileName,
        contentType: file.file.type
      },
      function (err, _id) {  // Callback to .insert
        if (err) { return console.error("File creation failed!", err); }
        // Once the file exists on the server, start uploading
        userPhotos.resumable.upload();
      }
      );
    });
  }

  render() {
    const {
      user, logoutHandler, changePhotoHandler, changeFileHandler,
      createUserHandler, updateUserHandler,
      avatar, context, users, cities, sexes, editModalWillShow,

      editCategoryModalWillShow, updateCategoryHandler,
      createCategoryHandler, removeCategoryHandler,

      editSubCategoryModalWillShow, updateSubCategoryHandler,
      createSubCategoryHandler, removeSubCategoryHandler,

      editBrandModalWillShow, updateBrandHandler,
      createBrandHandler, removeBrandHandler,

      editExtraFeatureModalWillShow, updateExtraFeatureHandler,
      createExtraFeatureHandler, removeExtraFeatureHandler,

      categories, subCategories, brands, extraFeatures
    } = this.props;

    let {hostname} = window.location;
    hostname = hostname == 'localhost' ? 'localhost:3000' : hostname;

    return (
      <div>
        <SuperAdminHelmet />
        <Header />
        <PageHeader
          pageName='Управление словарями'
          breadcrumbName='Словари'/>
        <section className="mainContent clearfix userProfile">
          <div className="container">
            <Navigation activeLink='dictionaries' />
            <div className="row">
              <div className="col-xs-12">
                <div className="innerWrapper profile">
                  <div className="orderBox">
                    <h4>Профиль</h4>
                  </div>
                  <div className="row">
                    <div className="col-md-2 col-sm-3 col-xs-12">
                      <div className="thumbnail">
                        <img
                          src={avatar ? `http://${hostname}/gridfs/userPhotos/${avatar.md5}` : "img/user.png"}
                          alt="profile-image" />
                        <div className="caption">
                          <a onClick={changePhotoHandler} href="#" className="btn btn-primary btn-block" role="button">Сменить Фото</a>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-10 col-sm-9 col-xs-12">
                      <div className="innerWrapper">
                        <div className="orderBox">
                          <div className="tab-section margin-bottom">
                            <div className="row">
                              <div className="col-xs-12">
                                <div className="tabCommon">
                                  <ul className="nav nav-tabs">
                                    <li className="active"><a data-toggle="tab" href="#menu1">Администраторы</a></li>
                                    <li><a data-toggle="tab" href="#menu2">Категории</a></li>
                                    <li><a data-toggle="tab" href="#menu3">Подкатегории</a></li>
                                    <li><a data-toggle="tab" href="#menu4">Брэнды</a></li>
                                    <li><a data-toggle="tab" href="#menu5">Характеристики</a></li>
                                  </ul>
                                  <div className="tab-bottom">
                                  </div>
                                  <div className="tab-content">
                                    <AdministratorsTab
                                      editModalWillShow={editModalWillShow}
                                      createUserHandler={createUserHandler}
                                      updateUserHandler={updateUserHandler}
                                      context={context}
                                      users={users}
                                    />
                                    <CategoriesTab
                                      editCategoryModalWillShow={editCategoryModalWillShow}
                                      updateCategoryHandler={updateCategoryHandler}
                                      createCategoryHandler={createCategoryHandler}
                                      removeCategoryHandler={removeCategoryHandler}
                                      context={context}
                                      categories={categories}
                                    />
                                    <SubCategoriesTab
                                      editSubCategoryModalWillShow={editSubCategoryModalWillShow}
                                      updateSubCategoryHandler={updateSubCategoryHandler}
                                      createSubCategoryHandler={createSubCategoryHandler}
                                      removeSubCategoryHandler={removeSubCategoryHandler}
                                      context={context}
                                      categories={categories}
                                      subCategories={subCategories}
                                    />
                                    <BrandsTab
                                      editBrandModalWillShow={editBrandModalWillShow}
                                      updateBrandHandler={updateBrandHandler}
                                      createBrandHandler={createBrandHandler}
                                      removeBrandHandler={removeBrandHandler}
                                      context={context}
                                      brands={brands}
                                    />
                                    <ExtraFeaturesTab
                                      editExtraFeatureModalWillShow={editExtraFeatureModalWillShow}
                                      updateExtraFeatureHandler={updateExtraFeatureHandler}
                                      createExtraFeatureHandler={createExtraFeatureHandler}
                                      removeExtraFeatureHandler={removeExtraFeatureHandler}
                                      context={context}
                                      extraFeatures={extraFeatures}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Copyright />
      </div>
    );
  }
}
