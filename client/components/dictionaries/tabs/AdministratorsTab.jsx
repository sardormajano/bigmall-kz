import React, {Component} from 'react';

export default class AdministratorsTab extends Component {

  getUsersJSX(usersArray) {
    const {removeUserHandler, editModalWillShow} = this.props;

    return usersArray.map((user, index) => {
      return (
        <tr key={index}>
          <td>{user._id}</td>
          <td>{user.username}</td>
          <td>{user.profile.firstName}</td>
          <td>{user.profile.lastName}</td>
          <td>
            <a
              data-id={user._id}
              data-toggle='modal'
              data-target='#edit-admin-modal'
              onClick={editModalWillShow}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-edit'></i>
            </a>&nbsp;&nbsp;
            <a
              data-id={user._id}
              onClick={removeUserHandler}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-remove'></i>
            </a>
          </td>
        </tr>
      );
    });
  }

  render() {
    const {
      user,
      createUserHandler, updateUserHandler,
      context, users
    } = this.props;

    return (
      <div id="menu1" className="tab-pane fade in active">
        <div className="orderBox">
          <h4>Управление администраторами</h4>
          <div className="col-sm-3">
            <a
              data-toggle='modal'
              data-target="#add-admin-modal"
              className="btn btn-info btn-block"
              role="button"
            >
              Добавить
            </a>
            <br/><br/>
          </div>
          <div className="table-responsive">
            <table className="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Логин / Email</th>
                  <th>Имя</th>
                  <th>Фамилия</th>
                  <th>Операции</th>
                </tr>
              </thead>
              <tbody>
                {this.getUsersJSX(users)}
              </tbody>
            </table>
          </div>
          <br />
          <div className="col-sm-3">
            <a
              data-toggle='modal'
              data-target="#add-admin-modal"
              className="btn btn-info btn-block"
              role="button"
            >
              Добавить
            </a>
          </div>
        </div>
        <div id="add-admin-modal" className="modal fade" tabIndex={-1} role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title" id="myModalLabel">Добавление админа</h4>
              </div>
              <div className="modal-body">
                  <form className="form-horizontal">
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Имя</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.firstName}
                          onChange={(e) => {context.setState({firstName: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Фамилия</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.lastName}
                          onChange={(e) => {context.setState({lastName: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Email</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.email}
                          onChange={(e) => {context.setState({email: e.currentTarget.value});}}
                          type="email"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Пароль</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.newPassword}
                          onChange={(e) => {context.setState({newPassword: e.currentTarget.value});}}
                          type="password"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-sm-4 pull-right">
                        <button
                          onClick={createUserHandler}
                          type="submit"
                          className="btn btn-primary btn-block"
                          data-dismiss="modal"
                        >
                          Сохранить
                        </button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
        <div id="edit-admin-modal" className="modal fade" tabIndex={-1} role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title" id="myModalLabel">Редактирование админа</h4>
              </div>
              <div className="modal-body">
                  <form className="form-horizontal">
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Имя</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.efirstName}
                          onChange={(e) => {context.setState({efirstName: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Фамилия</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.elastName}
                          onChange={(e) => {context.setState({elastName: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Email</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.eemail}
                          onChange={(e) => {context.setState({eemail: e.currentTarget.value});}}
                          type="email"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Новый пароль</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.enewPassword}
                          placeholder='оставьте поле пустым, если не хотите его менять'
                          onChange={(e) => {context.setState({enewPassword: e.currentTarget.value});}}
                          type="password"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-sm-4 pull-right">
                        <button
                          onClick={updateUserHandler}
                          type="submit"
                          className="btn btn-primary btn-block"
                          data-dismiss="modal"
                        >
                          Сохранить
                        </button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
