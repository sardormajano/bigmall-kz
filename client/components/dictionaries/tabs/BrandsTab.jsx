import React, {Component} from 'react';

export default class BrandsTab extends Component {

  getBrandsJSX(brandsArray) {
    const {removeBrandHandler, editBrandModalWillShow} = this.props;

    return brandsArray.map((brand, index) => {
      return (
        <tr key={index}>
          <td>{brand._id}</td>
          <td>{brand.name}</td>
          <td>
            <a
              data-id={brand._id}
              data-toggle='modal'
              data-target='#edit-brand-modal'
              onClick={editBrandModalWillShow}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-edit'></i>
            </a>&nbsp;&nbsp;
            <a
              data-id={brand._id}
              onClick={removeBrandHandler}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-remove'></i>
            </a>
          </td>
        </tr>
      );
    });
  }

  render() {
    const {
      createBrandHandler, updateBrandHandler,
      context, brands
    } = this.props;

    return (
      <div id="menu4" className="tab-pane fade">
          <div className="orderBox">
            <h4>Управление брэндами</h4>
            <div className="col-sm-3">
              <a
                data-toggle='modal'
                data-target="#add-brand-modal"
                className="btn btn-info btn-block"
                role="button"
              >
                Добавить
              </a>
              <br/><br/>
            </div>
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Операции</th>
                  </tr>
                </thead>
                <tbody>
                  {this.getBrandsJSX(brands)}
                </tbody>
              </table>
            </div>
            <br />
            <div className="col-sm-3">
              <a
                data-toggle='modal'
                data-target="#add-brand-modal"
                className="btn btn-info btn-block"
                role="button"
              >
                Добавить
              </a>
            </div>
          </div>
          <div id="add-brand-modal" className="modal fade" tabIndex={-1} role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 className="modal-title" id="myModalLabel">Добавление брэнда</h4>
                </div>
                <div className="modal-body">
                    <form className="form-horizontal">
                      <div className="form-group">
                        <label htmlFor className="col-md-2 col-sm-3 control-label">Название</label>
                        <div className="col-md-10 col-sm-9">
                          <input
                            value={context.state.brandName}
                            onChange={(e) => {context.setState({brandName: e.currentTarget.value});}}
                            type="text"
                            className="form-control"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="col-sm-4 pull-right">
                          <button
                            onClick={createBrandHandler}
                            type="submit"
                            className="btn btn-primary btn-block"
                            data-dismiss="modal"
                          >
                            Сохранить
                          </button>
                        </div>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
          <div id="edit-brand-modal" className="modal fade" tabIndex={-1} role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 className="modal-title" id="myModalLabel">Редактирование брэнда</h4>
                </div>
                <div className="modal-body">
                    <form className="form-horizontal">
                      <div className="form-group">
                        <label htmlFor className="col-md-2 col-sm-3 control-label">Название</label>
                        <div className="col-md-10 col-sm-9">
                          <input
                            value={context.state.ebrandName}
                            onChange={(e) => {context.setState({ebrandName: e.currentTarget.value});}}
                            type="text"
                            className="form-control"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="col-sm-4 pull-right">
                          <button
                            onClick={updateBrandHandler}
                            type="submit"
                            className="btn btn-primary btn-block"
                            data-dismiss="modal"
                          >
                            Сохранить
                          </button>
                        </div>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}
