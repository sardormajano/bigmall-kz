import React, {Component} from 'react';

export default class CategoriesTab extends Component {

  getCategoriesJSX(categoriesArray) {
    const {removeCategoryHandler, editCategoryModalWillShow} = this.props;

    return categoriesArray.map((category, index) => {
      return (
        <tr key={index}>
          <td>{category._id}</td>
          <td>{category.name}</td>
          <td>
            <a
              data-id={category._id}
              data-toggle='modal'
              data-target='#edit-category-modal'
              onClick={editCategoryModalWillShow}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-edit'></i>
            </a>&nbsp;&nbsp;
            <a
              data-id={category._id}
              onClick={removeCategoryHandler}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-remove'></i>
            </a>
          </td>
        </tr>
      );
    });
  }

  render() {
    const {
      createCategoryHandler, updateCategoryHandler,
      context, categories
    } = this.props;

    return (
      <div id="menu2" className="tab-pane fade">
          <div className="orderBox">
            <h4>Управление категориями</h4>
            <div className="col-sm-3">
              <a
                data-toggle='modal'
                data-target="#add-category-modal"
                className="btn btn-info btn-block"
                role="button"
              >
                Добавить
              </a>
              <br/><br/>
            </div>
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Операции</th>
                  </tr>
                </thead>
                <tbody>
                  {this.getCategoriesJSX(categories)}
                </tbody>
              </table>
            </div>
            <br />
            <div className="col-sm-3">
              <a
                data-toggle='modal'
                data-target="#add-category-modal"
                className="btn btn-info btn-block"
                role="button"
              >
                Добавить
              </a>
            </div>
          </div>
          <div id="add-category-modal" className="modal fade" tabIndex={-1} role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 className="modal-title" id="myModalLabel">Добавление категории</h4>
                </div>
                <div className="modal-body">
                    <form className="form-horizontal">
                      <div className="form-group">
                        <label htmlFor className="col-md-2 col-sm-3 control-label">Название</label>
                        <div className="col-md-10 col-sm-9">
                          <input
                            value={context.state.categoryName}
                            onChange={(e) => {context.setState({categoryName: e.currentTarget.value});}}
                            type="text"
                            className="form-control"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="col-sm-4 pull-right">
                          <button
                            onClick={createCategoryHandler}
                            type="submit"
                            className="btn btn-primary btn-block"
                            data-dismiss="modal"
                          >
                            Сохранить
                          </button>
                        </div>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
          <div id="edit-category-modal" className="modal fade" tabIndex={-1} role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 className="modal-title" id="myModalLabel">Редактирование категории</h4>
                </div>
                <div className="modal-body">
                    <form className="form-horizontal">
                      <div className="form-group">
                        <label htmlFor className="col-md-2 col-sm-3 control-label">Название</label>
                        <div className="col-md-10 col-sm-9">
                          <input
                            value={context.state.ecategoryName}
                            onChange={(e) => {context.setState({ecategoryName: e.currentTarget.value});}}
                            type="text"
                            className="form-control"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="col-sm-4 pull-right">
                          <button
                            onClick={updateCategoryHandler}
                            type="submit"
                            className="btn btn-primary btn-block"
                            data-dismiss="modal"
                          >
                            Сохранить
                          </button>
                        </div>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}
