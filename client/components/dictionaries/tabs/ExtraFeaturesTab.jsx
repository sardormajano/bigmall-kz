import React, {Component} from 'react';

export default class ExtraFeaturesTab extends Component {

  getExtraFeaturesJSX(extraFeaturesArray) {
    const {removeExtraFeatureHandler, editExtraFeatureModalWillShow} = this.props;

    return extraFeaturesArray.map((extraFeature, index) => {
      return (
        <tr key={index}>
          <td>{extraFeature._id}</td>
          <td>{extraFeature.name}</td>
          <td>
            <a
              data-id={extraFeature._id}
              data-toggle='modal'
              data-target='#edit-extraFeature-modal'
              onClick={editExtraFeatureModalWillShow}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-edit'></i>
            </a>&nbsp;&nbsp;
            <a
              data-id={extraFeature._id}
              onClick={removeExtraFeatureHandler}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-remove'></i>
            </a>
          </td>
        </tr>
      );
    });
  }

  render() {
    const {
      createExtraFeatureHandler, updateExtraFeatureHandler,
      context, extraFeatures
    } = this.props;

    return (
      <div id="menu5" className="tab-pane fade">
          <div className="orderBox">
            <h4>Управление дополнительными характеристиками</h4>
            <div className="col-sm-3">
              <a
                data-toggle='modal'
                data-target="#add-extraFeature-modal"
                className="btn btn-info btn-block"
                role="button"
              >
                Добавить
              </a>
              <br/><br/>
            </div>
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Операции</th>
                  </tr>
                </thead>
                <tbody>
                  {this.getExtraFeaturesJSX(extraFeatures)}
                </tbody>
              </table>
            </div>
            <br />
            <div className="col-sm-3">
              <a
                data-toggle='modal'
                data-target="#add-extraFeature-modal"
                className="btn btn-info btn-block"
                role="button"
              >
                Добавить
              </a>
            </div>
          </div>
          <div id="add-extraFeature-modal" className="modal fade" tabIndex={-1} role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 className="modal-title" id="myModalLabel">Добавление доп. характеристики</h4>
                </div>
                <div className="modal-body">
                    <form className="form-horizontal">
                      <div className="form-group">
                        <label htmlFor className="col-md-2 col-sm-3 control-label">Название</label>
                        <div className="col-md-10 col-sm-9">
                          <input
                            value={context.state.extraFeatureName}
                            onChange={(e) => {context.setState({extraFeatureName: e.currentTarget.value});}}
                            type="text"
                            className="form-control"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="col-sm-4 pull-right">
                          <button
                            onClick={createExtraFeatureHandler}
                            type="submit"
                            className="btn btn-primary btn-block"
                            data-dismiss="modal"
                          >
                            Сохранить
                          </button>
                        </div>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
          <div id="edit-extraFeature-modal" className="modal fade" tabIndex={-1} role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 className="modal-title" id="myModalLabel">Редактирование доп. характеристики</h4>
                </div>
                <div className="modal-body">
                    <form className="form-horizontal">
                      <div className="form-group">
                        <label htmlFor className="col-md-2 col-sm-3 control-label">Название</label>
                        <div className="col-md-10 col-sm-9">
                          <input
                            value={context.state.eextraFeatureName}
                            onChange={(e) => {context.setState({eextraFeatureName: e.currentTarget.value});}}
                            type="text"
                            className="form-control"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="col-sm-4 pull-right">
                          <button
                            onClick={updateExtraFeatureHandler}
                            type="submit"
                            className="btn btn-primary btn-block"
                            data-dismiss="modal"
                          >
                            Сохранить
                          </button>
                        </div>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}
