import React, {Component} from 'react';

import Select2 from '../../stateless/Select2';

import {qs} from '../../../lib/coreLib';

export default class SubCategoriesTab extends Component {

  getSubCategoriesJSX(subCategoriesArray) {
    const {removeSubCategoryHandler, editSubCategoryModalWillShow, categories} = this.props;

    return subCategoriesArray.map((subCategory, index) => {
      const currentCategory = categories.filter((category) => category._id === subCategory.category)[0];

      return (
        <tr key={index}>
          <td>{subCategory._id}</td>
          <td>{subCategory.name}</td>
          <td>{currentCategory.name}</td>
          <td>
            <a
              data-id={subCategory._id}
              data-toggle='modal'
              data-target='#edit-sub-category-modal'
              onClick={editSubCategoryModalWillShow}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-edit'></i>
            </a>&nbsp;&nbsp;
            <a
              data-id={subCategory._id}
              onClick={removeSubCategoryHandler}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-remove'></i>
            </a>
          </td>
        </tr>
      );
    });
  }

  render() {
    const {
      createSubCategoryHandler, updateSubCategoryHandler,
      context, categories, subCategories
    } = this.props;

    return (
      <div id="menu3" className="tab-pane fade">
          <div className="orderBox">
            <h4>Управление подкатегориями</h4>
            <div className="col-sm-3">
              <a
                data-toggle='modal'
                data-target="#add-sub-category-modal"
                className="btn btn-info btn-block"
                role="button"
              >
                Добавить
              </a>
              <br/><br/>
            </div>
            <div className="table-responsive">
              <table className="table">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Категория</th>
                    <th>Операции</th>
                  </tr>
                </thead>
                <tbody>
                  {this.getSubCategoriesJSX(subCategories)}
                </tbody>
              </table>
            </div>
            <br />
            <div className="col-sm-3">
              <a
                data-toggle='modal'
                data-target="#add-sub-category-modal"
                className="btn btn-info btn-block"
                role="button"
              >
                Добавить
              </a>
            </div>
          </div>
          <div id="add-sub-category-modal" className="modal fade" tabIndex={-1} role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 className="modal-title" id="myModalLabel">Добавление подкатегории</h4>
                </div>
                <div className="modal-body">
                    <form className="form-horizontal">
                      <div className="form-group">
                        <label htmlFor className="col-md-2 col-sm-3 control-label">Название</label>
                        <div className="col-md-10 col-sm-9">
                          <input
                            value={context.state.ssubCategoryName}
                            onChange={(e) => {context.setState({ssubCategoryName: e.currentTarget.value});}}
                            type="text"
                            className="form-control"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor className="col-md-2 col-sm-3 control-label">Категория</label>
                        <div className="col-md-10 col-sm-9">
                          <Select2
                            options={categories}
                            context={context}
                            label='Выберите категорию'
                            statename='scategory'
                            className="form-control"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="col-sm-4 pull-right">
                          <button
                            onClick={(e) => {
                              e.preventDefault();
                              const {scategory, ssubCategoryName} = context.state;
                              if(scategory !== '' && ssubCategoryName !== '')
                              {
                                qs('#sub-category-modal-dismiss').click();
                                createSubCategoryHandler.call(context, e);
                              }
                              else
                                Bert.alert( 'Все поля должны быть заполнены', 'danger', 'growl-top-right' );
                            }}
                            type="submit"
                            className="btn btn-primary btn-block"
                          >
                            Сохранить
                          </button>
                          <button className="hidden" id="sub-category-modal-dismiss" data-dismiss="modal"></button>
                        </div>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
          <div id="edit-sub-category-modal" className="modal fade" tabIndex={-1} role="dialog">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 className="modal-title" id="myModalLabel">Редактирование подкатегории</h4>
                </div>
                <div className="modal-body">
                    <form className="form-horizontal">
                      <div className="form-group">
                        <label htmlFor className="col-md-2 col-sm-3 control-label">Название</label>
                        <div className="col-md-10 col-sm-9">
                          <input
                            value={context.state.essubCategoryName}
                            onChange={(e) => {context.setState({essubCategoryName: e.currentTarget.value});}}
                            type="text"
                            className="form-control"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <label htmlFor className="col-md-2 col-sm-3 control-label">Категория</label>
                        <div className="col-md-10 col-sm-9">
                          <Select2
                            options={categories}
                            context={context}
                            label='Выберите категорию'
                            statename='escategory'
                            className="form-control"
                          />
                        </div>
                      </div>
                      <div className="form-group">
                        <div className="col-sm-4 pull-right">
                          <button
                            onClick={updateSubCategoryHandler}
                            type="submit"
                            className="btn btn-primary btn-block"
                            data-dismiss="modal"
                          >
                            Сохранить
                          </button>
                        </div>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}
