import React, {Component} from 'react';

import {Link} from 'react-router';

import TopBar from '../stateless/TopBar';
import SearchNav from '../stateless/SearchNav';
import Header from '../stateless/Header';
import PageHeader from '../stateless/PageHeader';
import Footer from '../stateless/Footer';
import Copyright from '../stateless/Copyright';

import {idToLink} from '../../lib/coreLib';

export default class Anonym extends Component {
  getNavCategoryJSX(categoryArray, subCategoryArray) {
    return categoryArray.map((citem, cindex) => {
      const subCategoriesJSX = subCategoryArray
        .filter((sitem) => sitem.category === citem._id)
        .map((sitem, sindex) => {
          return (
            <li key={sindex}><Link to={`/merchandise-search/${sitem._id}/-`}>{sitem.name}</Link></li>
          );
        });

      return (
        <li className="dropdown dropdown-submenu" key={cindex}>
          <Link to='#' className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            {citem.name}
          </Link>
          <ul className="dropdown-menu">
            {subCategoriesJSX}
          </ul>
        </li>
      );
    });
  }

  componentDidMount() {
    $.getScript('/plugins/owl-carousel/owl.carousel.js', () => {
      $('.owl-carousel.categorySlider').owlCarousel({
        loop:true,
        margin:28,
        autoplay:false,
        nav:true,
        moveSlides: 1,
        dots: false,
        smartSpeed:1000,
        responsive:{
          320:{
            items:1
          },
          768:{
            items:1
          },
          992:{
            items:1
          }
        }
      });
    });

    $.getScript('/plugins/rs-plugin/js/jquery.themepunch.tools.min.js', () => {
      $.getScript('/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js', () => {
        $('.bannerV4 .fullscreenbanner').revolution({
      		delay: 5000,
      		startwidth: 835,
      		startheight: 470,
      		fullWidth: "off",
      		fullScreen: "off",
      		hideCaptionAtLimit: "",
      		dottedOverlay: "twoxtwo",
      		navigationStyle: "preview4",
      		fullScreenOffsetContainer: "",
      		hideTimerBar:"on",
      		onHoverStop:"on",
      	});
      });
    });
  }

  render() {
    const {user, context} = this.props,
          {
      categories, subCategories,
      recommendedCollections, recommendedCategories,
      fullScreenSlides
    } = context.props;

    console.log(recommendedCollections, recommendedCategories, fullScreenSlides);

    return (
      <div>
        <div className="main-wrapper">
          {/* HEADER */}
          <div className="header clearfix headerV3">
            {/* TOPBAR */}
            <TopBar user={user}/>
            {/* NAV TOP */}
            <SearchNav context={context}/>
            {/* NAVBAR */}
            <nav className="navbar navbar-main navbar-default nav-V3" role="navigation">
              <div className="container">
                <div className="nav-category dropdown">
                  <a href="javascript:void(0)" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Категории
                    <button type="button">
                      <span className="icon-bar" />
                      <span className="icon-bar" />
                      <span className="icon-bar" />
                    </button>
                  </a>
                  <ul className="dropdown-menu dropdown-menu-left">
                    {this.getNavCategoryJSX(categories, subCategories)}
                    <li><a href="#"></a></li>
                  </ul>
                </div>
                <div className="navbar-header">
                  <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar" />
                    <span className="icon-bar" />
                    <span className="icon-bar" />
                  </button>
                </div>
                {/* Collect the nav links, forms, and other content for toggling */}
                <div className="collapse navbar-collapse navbar-ex1-collapse">
                  <ul className="nav navbar-nav">
                    <li><a href="#">Главная</a></li>
                    <li><a href="#">Магазины</a></li>
                    <li><a href="#">Товары</a></li>
                    <li><a href="#">О нас</a></li>
                  </ul>
                </div>{/* /.navbar-collapse */}
              </div>
            </nav>
          </div>
          {/* BANNER */}
          <div className="container">
            <div className="bannercontainer bannerV4">
              <div className="fullscreenbanner-container">
                <div className="fullscreenbanner">
                  <ul>
                    <li data-transition="slidehorizontal" data-slotamount={5} data-masterspeed={700} data-title="Slide 3">
                      <img src="/img/home/banner-slider/slider-img1.jpg" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" />
                      <div className="slider-caption slider-captionV4">
                        <div className="tp-caption rs-caption-2 sft" data-hoffset={0} data-x={85} data-y={115} data-speed={800} data-start={2000} data-easing="Back.easeInOut" data-endspeed={300}>
                          <small>Winter Offer</small><br />
                          Up To 50% off
                        </div>
                        <div className="tp-caption rs-caption-3 sft" data-hoffset={0} data-x={85} data-y={240} data-speed={1000} data-start={3000} data-easing="Power4.easeOut" data-endspeed={300} data-endeasing="Power1.easeIn" data-captionhidden="off">
                          Lorem ipsum dolor sit amet, consectetur adipisicing
                        </div>
                        <div className="tp-caption rs-caption-4 sft" data-hoffset={0} data-x={85} data-y={300} data-speed={800} data-start={3500} data-easing="Power4.easeOut" data-endspeed={300} data-endeasing="Power1.easeIn" data-captionhidden="off">
                          <span className="page-scroll"><a href="#" className="btn primary-btn">Buy Now<i className="glyphicon glyphicon-chevron-right" /></a></span>
                        </div>
                      </div>
                    </li>
                    <li data-transition="slidehorizontal" data-slotamount={5} data-masterspeed={700} data-title="Slide 1">
                      <img src="/img/home/banner-slider/slider-img3.jpg" alt="slidebg1" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" />
                      <div className="slider-caption slider-captionV4">
                        <div className="tp-caption rs-caption-2 sft" data-hoffset={0} data-x={85} data-y={115} data-speed={800} data-start={2000} data-easing="Back.easeInOut" data-endspeed={300}>
                          <small>Winter Offer</small><br />
                          Up To 50% off
                        </div>
                        <div className="tp-caption rs-caption-3 sft" data-hoffset={0} data-x={85} data-y={240} data-speed={1000} data-start={3000} data-easing="Power4.easeOut" data-endspeed={300} data-endeasing="Power1.easeIn" data-captionhidden="off">
                          Lorem ipsum dolor sit amet, consectetur adipisicing
                        </div>
                        <div className="tp-caption rs-caption-4 sft" data-hoffset={0} data-x={85} data-y={300} data-speed={800} data-start={3500} data-easing="Power4.easeOut" data-endspeed={300} data-endeasing="Power1.easeIn" data-captionhidden="off">
                          <span className="page-scroll"><a href="#" className="btn primary-btn">Buy Now<i className="glyphicon glyphicon-chevron-right" /></a></span>
                        </div>
                      </div>
                    </li>
                    <li data-transition="slidehorizontal" data-slotamount={5} data-masterspeed={1000} data-title="Slide 2">
                      <img src="/img/home/banner-slider/slider-img2.jpg" alt="slidebg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" />
                      <div className="slider-caption slider-captionV4 text-center">
                        <div className="tp-caption rs-caption-2 sft text-center" data-x="center" data-y={200} data-speed={800} data-start={2000} data-easing="Back.easeInOut" data-endspeed={300}>
                          <small>Winter Offer</small><br />
                          Up To 50% off
                        </div>
                        <div className="tp-caption rs-caption-3 sft text-center" data-x="center" data-y={325} data-speed={1000} data-start={3000} data-easing="Power4.easeOut" data-endspeed={300} data-endeasing="Power1.easeIn" data-captionhidden="off">
                          Lorem ipsum dolor sit amet, consectetur adipisicing
                        </div>
                        <div className="tp-caption rs-caption-4 sft text-center" data-x="center" data-y={385} data-speed={800} data-start={3500} data-easing="Power4.easeOut" data-endspeed={300} data-endeasing="Power1.easeIn" data-captionhidden="off">
                          <span className="page-scroll"><a href="#" className="btn primary-btn">Buy Now<i className="glyphicon glyphicon-chevron-right" /></a></span>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          {/* CONTENT SECTION */}
          <section className="content clearfix">
            <div className="container">
              {/* FEATURE COLLECTION SECTION */}
              <div className="row featuredCollection version2 version3">
                <div className="col-sm-6 col-xs-12">
                  <div className="slide">
                    <div className="productImage productImage1">
                    </div>
                    <div className="productCaption clearfix text-right">
                      <h3><a href="single-product.html">women’s Shoes</a></h3>
                      <p>Lorem ipsum dolor sit ameit</p>
                      <a href="single-product.html" className="btn btn-border">Shop Now</a>
                    </div>
                  </div>
                </div>
                <div className="col-sm-6 col-xs-12">
                  <div className="slide">
                    <div className="productImage productImage2">
                    </div>
                    <div className="productCaption clearfix text-right">
                      <h3><a href="single-product.html">Headphone wifi</a></h3>
                      <p>Lorem ipsum dolor sit ameit</p>
                      <a href="single-product.html" className="btn btn-border">Shop Now</a>
                    </div>
                  </div>
                </div>
              </div>
              {/* CATEGORY SECTION */}
              <div className="categorySection">
                <div className="row">
                  <div className="col-md-6 col-xs-12">
                    <div className="category-content">
                      <div className="category-top">
                        <div className="category-menu text-center">
                          <h2 className="category-title">Men’s Clothing</h2>
                          <ul>
                            <li><a href="product-grid-left-sidebar.html">Down Jackets</a></li>
                            <li><a href="product-grid-left-sidebar.html">T Shirts</a></li>
                            <li><a href="product-grid-left-sidebar.html">Jeans</a></li>
                            <li><a href="product-grid-left-sidebar.html">Casual Pants</a></li>
                            <li><a href="product-grid-left-sidebar.html">Sunglasses</a></li>
                            <li><a href="product-grid-left-sidebar.html">Hoodies</a></li>
                          </ul>
                        </div>
                        <div className="category-Slider">
                          <div className="owl-carousel categorySlider">
                            <div className="item">
                              <img src="/img/home/category/category-img3.jpg" alt="Image" />
                            </div>
                            <div className="item">
                              <img src="/img/home/category/category-img3.jpg" alt="Image" />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="category-bottom">
                        <div className="imageBox">
                          <div className="productImage clearfix">
                            <a href="single-product.html">
                              <img src="/img/home/category/category-img6.jpg" alt="Image" />
                            </a>
                            <div className="productMasking">
                              <ul className="list-inline btn-group" role="group">
                                <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                                <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                                <li><a data-toggle="modal" href=".quick-view" className="btn btn-default"><i className="fa fa-search" /></a></li>
                              </ul>
                            </div>
                          </div>
                          <div className="productCaption clearfix">
                            <h5><a href="single-product.html">Nike Sportswear</a></h5>
                            <h3>$199</h3>
                          </div>
                        </div>
                        <div className="imageBox">
                          <div className="productImage clearfix">
                            <a href="single-product.html">
                              <img src="/img/home/category/category-img7.jpg" alt="Image" />
                            </a>
                            <span className="sticker">50% off</span>
                            <div className="productMasking">
                              <ul className="list-inline btn-group" role="group">
                                <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                                <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                                <li><a data-toggle="modal" href=".quick-view" className="btn btn-default"><i className="fa fa-search" /></a></li>
                              </ul>
                            </div>
                          </div>
                          <div className="productCaption clearfix">
                            <h5><a href="single-product.html">Nike Sportswear</a></h5>
                            <h3>$199</h3>
                          </div>
                        </div>
                        <div className="imageBox">
                          <div className="productImage clearfix">
                            <a href="single-product.html">
                              <img src="/img/home/category/category-img8.jpg" alt="Image" />
                            </a>
                            <div className="productMasking">
                              <ul className="list-inline btn-group" role="group">
                                <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                                <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                                <li><a data-toggle="modal" href=".quick-view" className="btn btn-default"><i className="fa fa-search" /></a></li>
                              </ul>
                            </div>
                          </div>
                          <div className="productCaption clearfix">
                            <h5><a href="single-product.html">Nike Sportswear</a></h5>
                            <h3>$199</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 col-xs-12">
                    <div className="category-content">
                      <div className="category-top">
                        <div className="category-menu text-center">
                          <h2 className="category-title">woMen’s Clothing</h2>
                          <ul>
                            <li><a href="product-grid-left-sidebar.html">Casual Dresses</a></li>
                            <li><a href="product-grid-left-sidebar.html">Sweaters</a></li>
                            <li><a href="product-grid-left-sidebar.html">Coats &amp; Jackets</a></li>
                            <li><a href="product-grid-left-sidebar.html">Cosmetic</a></li>
                            <li><a href="product-grid-left-sidebar.html">T Shirts</a></li>
                            <li><a href="product-grid-left-sidebar.html">Shoes</a></li>
                          </ul>
                        </div>
                        <div className="category-Slider">
                          <div className="owl-carousel categorySlider">
                            <div className="item">
                              <img src="/img/home/category/category-img2.jpg" alt="Image" />
                            </div>
                            <div className="item">
                              <img src="/img/home/category/category-img2.jpg" alt="Image" />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="category-bottom">
                        <div className="imageBox">
                          <div className="productImage clearfix">
                            <a href="single-product.html">
                              <img src="/img/home/category/category-img9.jpg" alt="Image" />
                            </a>
                            <span className="sticker">50% off</span>
                            <div className="productMasking">
                              <ul className="list-inline btn-group" role="group">
                                <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                                <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                                <li><a data-toggle="modal" href=".quick-view" className="btn btn-default"><i className="fa fa-search" /></a></li>
                              </ul>
                            </div>
                          </div>
                          <div className="productCaption clearfix">
                            <h5><a href="single-product.html">Nike Sportswear</a></h5>
                            <h3>$199</h3>
                          </div>
                        </div>
                        <div className="imageBox">
                          <div className="productImage clearfix">
                            <a href="single-product.html">
                              <img src="/img/home/category/category-img10.jpg" alt="Image" />
                            </a>
                            <div className="productMasking">
                              <ul className="list-inline btn-group" role="group">
                                <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                                <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                                <li><a data-toggle="modal" href=".quick-view" className="btn btn-default"><i className="fa fa-search" /></a></li>
                              </ul>
                            </div>
                          </div>
                          <div className="productCaption clearfix">
                            <h5><a href="single-product.html">Nike Sportswear</a></h5>
                            <h3>$199</h3>
                          </div>
                        </div>
                        <div className="imageBox">
                          <div className="productImage clearfix">
                            <a href="single-product.html">
                              <img src="/img/home/category/category-img11.jpg" alt="Image" />
                            </a>
                            <div className="productMasking">
                              <ul className="list-inline btn-group" role="group">
                                <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                                <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                                <li><a data-toggle="modal" href=".quick-view" className="btn btn-default"><i className="fa fa-search" /></a></li>
                              </ul>
                            </div>
                          </div>
                          <div className="productCaption clearfix">
                            <h5><a href="single-product.html">Nike Sportswear</a></h5>
                            <h3>$199</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 col-xs-12">
                    <div className="category-content">
                      <div className="category-top">
                        <div className="category-menu text-center">
                          <h2 className="category-title">Jewelry Collections</h2>
                          <ul>
                            <li><a href="product-grid-left-sidebar.html">Jewlery Sets</a></li>
                            <li><a href="product-grid-left-sidebar.html">Earring Sets</a></li>
                            <li><a href="product-grid-left-sidebar.html">DIY Jewelry</a></li>
                            <li><a href="product-grid-left-sidebar.html">Pendant Nacklaces</a></li>
                            <li><a href="product-grid-left-sidebar.html">Popupar Bracelets</a></li>
                            <li><a href="product-grid-left-sidebar.html">Pendant</a></li>
                          </ul>
                        </div>
                        <div className="category-Slider">
                          <div className="owl-carousel categorySlider">
                            <div className="item">
                              <img src="/img/home/category/category-img4.jpg" alt="Image" />
                            </div>
                            <div className="item">
                              <img src="/img/home/category/category-img4.jpg" alt="Image" />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="category-bottom">
                        <div className="imageBox">
                          <div className="productImage clearfix">
                            <a href="single-product.html">
                              <img src="/img/home/category/category-img13.jpg" alt="Image" />
                            </a>
                            <span className="sticker">50% off</span>
                            <div className="productMasking">
                              <ul className="list-inline btn-group" role="group">
                                <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                                <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                                <li><a data-toggle="modal" href=".quick-view" className="btn btn-default"><i className="fa fa-search" /></a></li>
                              </ul>
                            </div>
                          </div>
                          <div className="productCaption clearfix">
                            <h5><a href="single-product.html">Nike Sportswear</a></h5>
                            <h3>$199</h3>
                          </div>
                        </div>
                        <div className="imageBox">
                          <div className="productImage clearfix">
                            <a href="single-product.html">
                              <img src="/img/home/category/category-img14.jpg" alt="Image" />
                            </a>
                            <div className="productMasking">
                              <ul className="list-inline btn-group" role="group">
                                <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                                <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                                <li><a data-toggle="modal" href=".quick-view" className="btn btn-default"><i className="fa fa-search" /></a></li>
                              </ul>
                            </div>
                          </div>
                          <div className="productCaption clearfix">
                            <h5><a href="single-product.html">Nike Sportswear</a></h5>
                            <h3>$199</h3>
                          </div>
                        </div>
                        <div className="imageBox">
                          <div className="productImage clearfix">
                            <a href="single-product.html">
                              <img src="/img/home/category/category-img12.jpg" alt="Image" />
                            </a>
                            <div className="productMasking">
                              <ul className="list-inline btn-group" role="group">
                                <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                                <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                                <li><a data-toggle="modal" href=".quick-view" className="btn btn-default"><i className="fa fa-search" /></a></li>
                              </ul>
                            </div>
                          </div>
                          <div className="productCaption clearfix">
                            <h5><a href="single-product.html">Nike Sportswear</a></h5>
                            <h3>$199</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6 col-xs-12">
                    <div className="category-content">
                      <div className="category-top">
                        <div className="category-menu text-center">
                          <h2 className="category-title">Bags &amp; Shoes</h2>
                          <ul>
                            <li><a href="product-grid-left-sidebar.html">Women’s Boots</a></li>
                            <li><a href="product-grid-left-sidebar.html">Women’s Casual Shoes</a></li>
                            <li><a href="product-grid-left-sidebar.html">Women’s Bags</a></li>
                            <li><a href="product-grid-left-sidebar.html">Men’s Boots</a></li>
                            <li><a href="product-grid-left-sidebar.html">Men’s Casual Shoes</a></li>
                            <li><a href="product-grid-left-sidebar.html">Men’s Bags</a></li>
                          </ul>
                        </div>
                        <div className="category-Slider">
                          <div className="owl-carousel categorySlider">
                            <div className="item">
                              <img src="/img/home/category/category-img5.jpg" alt="Image" />
                            </div>
                            <div className="item">
                              <img src="/img/home/category/category-img5.jpg" alt="Image" />
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="category-bottom">
                        <div className="imageBox">
                          <div className="productImage clearfix">
                            <a href="single-product.html">
                              <img src="/img/home/category/category-img16.jpg" alt="Image" />
                            </a>
                            <span className="sticker">50% off</span>
                            <div className="productMasking">
                              <ul className="list-inline btn-group" role="group">
                                <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                                <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                                <li><a data-toggle="modal" href=".quick-view" className="btn btn-default"><i className="fa fa-search" /></a></li>
                              </ul>
                            </div>
                          </div>
                          <div className="productCaption clearfix">
                            <h5><a href="single-product.html">Nike Sportswear</a></h5>
                            <h3>$199</h3>
                          </div>
                        </div>
                        <div className="imageBox">
                          <div className="productImage clearfix">
                            <a href="single-product.html">
                              <img src="/img/home/category/category-img17.jpg" alt="Image" />
                            </a>
                            <div className="productMasking">
                              <ul className="list-inline btn-group" role="group">
                                <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                                <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                                <li><a data-toggle="modal" href=".quick-view" className="btn btn-default"><i className="fa fa-search" /></a></li>
                              </ul>
                            </div>
                          </div>
                          <div className="productCaption clearfix">
                            <h5><a href="single-product.html">Nike Sportswear</a></h5>
                            <h3>$199</h3>
                          </div>
                        </div>
                        <div className="imageBox">
                          <div className="productImage clearfix">
                            <a href="single-product.html">
                              <img src="/img/home/category/category-img15.jpg" alt="Image" />
                            </a>
                            <div className="productMasking">
                              <ul className="list-inline btn-group" role="group">
                                <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                                <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                                <li><a data-toggle="modal" href=".quick-view" className="btn btn-default"><i className="fa fa-search" /></a></li>
                              </ul>
                            </div>
                          </div>
                          <div className="productCaption clearfix">
                            <h5><a href="single-product.html">Nike Sportswear</a></h5>
                            <h3>$199</h3>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <Footer />
          <Copyright />
        </div>
        {/* PORDUCT QUICK VIEW MODAL */}
        <div className="modal fade quick-view" tabIndex={-1} role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-body">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div className="media">
                  <div className="media-left">
                    <img className="media-object" src="/img/products/quick-view/quick-view-01.jpg" alt="Image" />
                  </div>
                  <div className="media-body">
                    <h2>Old Skool Navy</h2>
                    <h3>$149</h3>
                    <p>Classic sneaker from Vans. Cotton canvas and suede upper. Textile lining with heel stabilizer and ankle support. Contrasting laces. Sits on a classic waffle rubber sole.</p>
                    <span className="quick-drop">
                      <select name="guiest_id3" id="guiest_id3" className="select-drop">
                        <option value={0}>Size</option>
                        <option value={1}>Size 1</option>
                        <option value={2}>Size 2</option>
                        <option value={3}>Size 3</option>
                      </select>
                    </span>
                    <span className="quick-drop resizeWidth">
                      <select name="guiest_id4" id="guiest_id4" className="select-drop">
                        <option value={0}>Qty</option>
                        <option value={1}>Qty 1</option>
                        <option value={2}>Qty 2</option>
                        <option value={3}>Qty 3</option>
                      </select>
                    </span>
                    <div className="btn-area">
                      <a href="#" className="btn btn-primary btn-block">Add to cart <i className="fa fa-angle-right" aria-hidden="true" /></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
