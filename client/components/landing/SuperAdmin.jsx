import React, {Component} from 'react';

import Header from '../stateless/Header';
import PageHeader from '../stateless/PageHeader';
import Footer from '../stateless/Footer';
import Copyright from '../stateless/Copyright';
import Navigation from '../stateless/Navigation';

import FullScreenSliderTab from './tabs/FullScreenSliderTab';
import RecommendedCollectionsTab from './tabs/RecommendedCollectionsTab';
import RecommendedCategoriesTab from './tabs/RecommendedCategoriesTab';

export default class SuperAdmin extends Component {
  render() {
    const {
      context,
      avatar,
      changePhotoHandler,
      fssPhotos,
      rclPhotos,
      rctPhotos
    } = this.props;

    let {hostname} = window.location;
    hostname = hostname == 'localhost' ? 'localhost:3000' : hostname;

    return (
      <div>
        <Header />
        <PageHeader
          pageName='Управление слайдами'
          breadcrumbName='Управление лэндингом'/>
        <section className="mainContent clearfix userProfile">
          <div className="container">
            <Navigation activeLink='landing' />
            <div className="row">
              <div className="col-xs-12">
                <div className="innerWrapper profile">
                  <div className="orderBox">
                    <h4>Профиль</h4>
                  </div>
                  <div className="row">
                    <div className="col-md-2 col-sm-3 col-xs-12">
                      <div className="thumbnail">
                        <img
                          src={avatar ? `http://${hostname}/gridfs/userPhotos/${avatar.md5}` : "img/user.png"}
                          alt="profile-image" />
                        <div className="caption">
                          <a onClick={changePhotoHandler} href="#" className="btn btn-primary btn-block" role="button">Сменить Фото</a>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-10 col-sm-9 col-xs-12">
                      <div className="innerWrapper">
                        <div className="orderBox">
                          <div className="tab-section margin-bottom">
                            <div className="row">
                              <div className="col-xs-12">
                                <div className="tabCommon">
                                  <ul className="nav nav-tabs">
                                    <li className="active"><a data-toggle="tab" href="#menu1">Главный слайдер</a></li>
                                    <li><a data-toggle="tab" href="#menu2">Рекомендованные коллекции</a></li>
                                    <li><a data-toggle="tab" href="#menu3">Рекомендованные категории</a></li>
                                  </ul>
                                  <div className="tab-bottom">
                                  </div>
                                  <div className="tab-content">
                                    <FullScreenSliderTab
                                      context={context}
                                      fss={context.fss}
                                      fssPhotos={fssPhotos}
                                    />
                                    <RecommendedCollectionsTab
                                      context={context}
                                      rcl={context.rcl}
                                      rclPhotos={rclPhotos}
                                    />
                                    <RecommendedCategoriesTab
                                      context={context}
                                      rct={context.rсt}
                                      rctPhotos={rctPhotos}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Copyright />
      </div>
    );
  }
}
