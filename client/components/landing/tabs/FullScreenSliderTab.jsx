import React, {Component} from 'react';

import {qs} from '../../../lib/coreLib';

export default class FullScreenSliderTab extends Component {

  componentDidMount() {
    const {fssPhotos, context} = this.props;

    fssPhotos.resumable.assignBrowse($("#fss-photo-input"));

    fssPhotos.resumable.on('fileAdded', (file) => {
      context.setState({
        fssPhoto: {
          fileName: file.fileName,
          _id: file.uniqueIdentifier
        }
      });

      fssPhotos.insert({
          _id: file.uniqueIdentifier,  // This is the ID resumable will use
          filename: file.fileName,
          contentType: file.file.type
        },
        function (err, _id) {  // Callback to .insert
          if (err) { return console.error("File creation failed!", err); }
          // Once the file exists on the server, start uploading
          fssPhotos.resumable.upload();
        }
      );
    });
  }

  getFullScreenSlidesJSX(fullScreenSlidesArray) {
    const {context} = this.props;

    return fullScreenSlidesArray.map((fss, index) => {
      return (
        <tr key={index}>
          <td>{fss._id}</td>
          <td>{fss.title}</td>
          <td>{fss.subtitle}</td>
          <td>
            <a
              data-id={fss._id}
              data-toggle='modal'
              data-target='#edit-full-scrren-slide-modal'
              onClick={context.editFssModalWillShow.bind(context)}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-edit'></i>
            </a>&nbsp;&nbsp;
            <a
              data-id={fss._id}
              onClick={context.removeFssHandler.bind(context)}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-remove'></i>
            </a>
          </td>
        </tr>
      );
    });
  }

  render() {
    const {
      user,
      context
    } = this.props;

    return (
      <div id="menu1" className="tab-pane fade in active">
        <div className="orderBox">
          <h4>Управление главным слайдером</h4>
          <div className="col-sm-3">
            <a
              data-toggle='modal'
              data-target="#add-full-scrren-slide-modal"
              className="btn btn-info btn-block"
              role="button"
            >
              Добавить
            </a>
            <br/><br/>
          </div>
          <div className="table-responsive">
            <table className="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Заголовок</th>
                  <th>Подзаголовок</th>
                  <th>Операции</th>
                </tr>
              </thead>
              <tbody>
                {this.getFullScreenSlidesJSX(context.props.fss)}
              </tbody>
            </table>
          </div>
          <br />
          <div className="col-sm-3">
            <a
              data-toggle='modal'
              data-target="#add-admin-modal"
              className="btn btn-info btn-block"
              role="button"
            >
              Добавить
            </a>
          </div>
        </div>
        <div id="add-full-scrren-slide-modal" className="modal fade" tabIndex={-1} role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title" id="myModalLabel">Добавление слайда</h4>
              </div>
              <div className="modal-body">
                  <form className="form-horizontal">
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Фото</label>
                      <div className="col-md-10 col-sm-9">
                        <a
                          onClick={
                            (e) => {
                              e.preventDefault();
                              qs('#fss-photo-input').click();
                            }
                          }
                          href="#"
                          className="btn btn-primary btn-block"
                          role="button"
                        >
                          {context.state.fssPhoto.fileName ? context.state.fssPhoto.fileName : 'Загрузить Фото'}
                        </a>
                      </div>
                      <div className="hidden">
                        <input
                          id="fss-photo-input"
                          type="file"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Заголовок</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.fssTitle}
                          onChange={(e) => {context.setState({fssTitle: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Подзаголовок</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.fssSubtitle}
                          onChange={(e) => {context.setState({fssSubtitle: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Текст</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.fssText}
                          onChange={(e) => {context.setState({fssText: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-sm-4 pull-right">
                        <button
                          onClick={context.createFssHandler.bind(context)}
                          type="submit"
                          className="btn btn-primary btn-block"
                          data-dismiss="modal"
                        >
                          Сохранить
                        </button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
        <div id="edit-full-scrren-slide-modal" className="modal fade" tabIndex={-1} role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title" id="myModalLabel">Редактирование слайда</h4>
              </div>
              <div className="modal-body">
                  <form className="form-horizontal">
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Заголовок</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.efssTitle}
                          onChange={(e) => {context.setState({efssTitle: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Подзаголовок</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.efssSubtitle}
                          onChange={(e) => {context.setState({efssSubtitle: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Текст</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.efssText}
                          onChange={(e) => {context.setState({efssText: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-sm-4 pull-right">
                        <button
                          onClick={context.updateFssHandler.bind(context)}
                          type="submit"
                          className="btn btn-primary btn-block"
                          data-dismiss="modal"
                        >
                          Сохранить
                        </button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
