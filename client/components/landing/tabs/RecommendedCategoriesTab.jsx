import React, {Component} from 'react';

import {qs} from '../../../lib/coreLib';

import Select2 from '../../stateless/Select2';

export default class RecommendedCategoriesTab extends Component {

  componentDidMount() {
    const {rctPhotos, context} = this.props;

    rctPhotos.resumable.assignBrowse($("#rct-photo-input"));

    rctPhotos.resumable.on('fileAdded', (file) => {
      context.setState({
        rctPhoto: {
          fileName: file.fileName,
          _id: file.uniqueIdentifier
        }
      });

      rctPhotos.insert({
          _id: file.uniqueIdentifier,  // This is the ID resumable will use
          filename: file.fileName,
          contentType: file.file.type
        },
        function (err, _id) {  // Callback to .insert
          if (err) { return console.error("File creation failed!", err); }
          // Once the file exists on the server, start uploading
          rctPhotos.resumable.upload();
        }
      );
    });
  }

  getRecommendedCategoriesJSX(fullScreenSlidesArray) {
    const {context} = this.props,
          {categories} = context.props;

    return fullScreenSlidesArray.map((rct, index) => {
      const categoryName = categories.filter((item) => item._id === rct.category)[0].name;

      return (
        <tr key={index}>
          <td>{rct._id}</td>
          <td>{categoryName}</td>
          <td>{rct.photo.fileName}</td>
          <td>
            <a
              data-id={rct._id}
              data-toggle='modal'
              data-target='#edit-recommended-category-modal'
              onClick={context.editRctModalWillShow.bind(context)}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-edit'></i>
            </a>&nbsp;&nbsp;
            <a
              data-id={rct._id}
              onClick={context.removeRctHandler.bind(context)}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-remove'></i>
            </a>
          </td>
        </tr>
      );
    });
  }

  render() {
    const { user, context } = this.props,
          {categories} = context.props;

    return (
      <div id="menu3" className="tab-pane">
        <div className="orderBox">
          <h4>Управление рекомендованными категориями</h4>
          <div className="col-sm-3">
            <a
              data-toggle='modal'
              data-target="#add-recommended-category-modal"
              className="btn btn-info btn-block"
              role="button"
            >
              Добавить
            </a>
            <br/><br/>
          </div>
          <div className="table-responsive">
            <table className="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Категория</th>
                  <th>Фото</th>
                  <th>Операции</th>
                </tr>
              </thead>
              <tbody>
                {this.getRecommendedCategoriesJSX(context.props.rct)}
              </tbody>
            </table>
          </div>
          <br />
          <div className="col-sm-3">
            <a
              data-toggle='modal'
              data-target="#add-admin-modal"
              className="btn btn-info btn-block"
              role="button"
            >
              Добавить
            </a>
          </div>
        </div>
        <div id="add-recommended-category-modal" className="modal fade" tabIndex={-1} role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title" id="myModalLabel">Добавление рекомендованной категории</h4>
              </div>
              <div className="modal-body">
                  <form className="form-horizontal">
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Фото</label>
                      <div className="col-md-10 col-sm-9">
                        <a
                          onClick={
                            (e) => {
                              e.preventDefault();
                              qs('#rct-photo-input').click();
                            }
                          }
                          href="#"
                          className="btn btn-primary btn-block"
                          role="button"
                        >
                          {context.state.rctPhoto.fileName ? context.state.rctPhoto.fileName : 'Загрузить Фото'}
                        </a>
                      </div>
                      <div className="hidden">
                        <input
                          id="rct-photo-input"
                          type="file"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Категория</label>
                      <div className="col-md-10 col-sm-9">
                        <Select2
                          options={categories}
                          context={context}
                          label='Выберите категорию'
                          statename='rctCategory'
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-sm-4 pull-right">
                        <button
                          onClick={context.createRctHandler.bind(context)}
                          type="submit"
                          className="btn btn-primary btn-block"
                          data-dismiss="modal"
                        >
                          Сохранить
                        </button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
        <div id="edit-recommended-category-modal" className="modal fade" tabIndex={-1} role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title" id="myModalLabel">Редактирование рекоммендованной категории</h4>
              </div>
              <div className="modal-body">
                  <form className="form-horizontal">
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Фото</label>
                      <div className="col-md-10 col-sm-9">
                        <a
                          onClick={
                            (e) => {
                              e.preventDefault();
                              qs('#rct-photo-input').click();
                            }
                          }
                          href="#"
                          className="btn btn-primary btn-block"
                          role="button"
                        >
                          {context.state.erctPhoto.fileName ? context.state.erctPhoto.fileName : 'Загрузить Фото'}
                        </a>
                      </div>
                      <div className="hidden">
                        <input
                          id="erct-photo-input"
                          type="file"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Категория</label>
                      <div className="col-md-10 col-sm-9">
                        <Select2
                          options={categories}
                          context={context}
                          label='Выберите категорию'
                          statename='erctCategory'
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-sm-4 pull-right">
                        <button
                          onClick={context.updateRctHandler.bind(context)}
                          type="submit"
                          className="btn btn-primary btn-block"
                          data-dismiss="modal"
                        >
                          Сохранить
                        </button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
