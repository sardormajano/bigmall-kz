import React, {Component} from 'react';

import {qs} from '../../../lib/coreLib';

export default class RecommendedCollectionsTab extends Component {

  componentDidMount() {
    const {rclPhotos, context} = this.props;

    rclPhotos.resumable.assignBrowse($("#rcl-photo-input"));

    rclPhotos.resumable.on('fileAdded', (file) => {
      context.setState({
        rclPhoto: {
          fileName: file.fileName,
          _id: file.uniqueIdentifier
        }
      });

      rclPhotos.insert({
          _id: file.uniqueIdentifier,  // This is the ID resumable will use
          filename: file.fileName,
          contentType: file.file.type
        },
        function (err, _id) {  // Callback to .insert
          if (err) { return console.error("File creation failed!", err); }
          // Once the file exists on the server, start uploading
          rclPhotos.resumable.upload();
        }
      );
    });
  }

  getRecommendedCollectionsJSX(fullScreenSlidesArray) {
    const {context} = this.props;

    return fullScreenSlidesArray.map((rcl, index) => {
      return (
        <tr key={index}>
          <td>{rcl._id}</td>
          <td>{rcl.title}</td>
          <td>{rcl.subtitle}</td>
          <td>
            <a
              data-id={rcl._id}
              data-toggle='modal'
              data-target='#edit-recommended-collection-modal'
              onClick={context.editRclModalWillShow.bind(context)}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-edit'></i>
            </a>&nbsp;&nbsp;
            <a
              data-id={rcl._id}
              onClick={context.removeRclHandler.bind(context)}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-remove'></i>
            </a>
          </td>
        </tr>
      );
    });
  }

  render() {
    const {
      user,
      context
    } = this.props;

    return (
      <div id="menu2" className="tab-pane">
        <div className="orderBox">
          <h4>Управление рекомендованными коллекциями</h4>
          <div className="col-sm-3">
            <a
              data-toggle='modal'
              data-target="#add-recommended-collection-modal"
              className="btn btn-info btn-block"
              role="button"
            >
              Добавить
            </a>
            <br/><br/>
          </div>
          <div className="table-responsive">
            <table className="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Заголовок</th>
                  <th>Подзаголовок</th>
                  <th>Операции</th>
                </tr>
              </thead>
              <tbody>
                {this.getRecommendedCollectionsJSX(context.props.rcl)}
              </tbody>
            </table>
          </div>
          <br />
          <div className="col-sm-3">
            <a
              data-toggle='modal'
              data-target="#add-admin-modal"
              className="btn btn-info btn-block"
              role="button"
            >
              Добавить
            </a>
          </div>
        </div>
        <div id="add-recommended-collection-modal" className="modal fade" tabIndex={-1} role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title" id="myModalLabel">Добавление рекомендованной коллекции</h4>
              </div>
              <div className="modal-body">
                  <form className="form-horizontal">
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Фото</label>
                      <div className="col-md-10 col-sm-9">
                        <a
                          onClick={
                            (e) => {
                              e.preventDefault();
                              qs('#rcl-photo-input').click();
                            }
                          }
                          href="#"
                          className="btn btn-primary btn-block"
                          role="button"
                        >
                          {context.state.rclPhoto.fileName ? context.state.rclPhoto.fileName : 'Загрузить Фото'}
                        </a>
                      </div>
                      <div className="hidden">
                        <input
                          id="rcl-photo-input"
                          type="file"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Заголовок</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.rclTitle}
                          onChange={(e) => {context.setState({rclTitle: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Подзаголовок</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.rclSubtitle}
                          onChange={(e) => {context.setState({rclSubtitle: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-sm-4 pull-right">
                        <button
                          onClick={context.createRclHandler.bind(context)}
                          type="submit"
                          className="btn btn-primary btn-block"
                          data-dismiss="modal"
                        >
                          Сохранить
                        </button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
        <div id="edit-recommended-collection-modal" className="modal fade" tabIndex={-1} role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title" id="myModalLabel">Редактирование рекоммендованной коллекции</h4>
              </div>
              <div className="modal-body">
                  <form className="form-horizontal">
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Заголовок</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.erclTitle}
                          onChange={(e) => {context.setState({erclTitle: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor className="col-md-2 col-sm-3 control-label">Подзаголовок</label>
                      <div className="col-md-10 col-sm-9">
                        <input
                          value={context.state.erclSubtitle}
                          onChange={(e) => {context.setState({erclSubtitle: e.currentTarget.value});}}
                          type="text"
                          className="form-control"
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="col-sm-4 pull-right">
                        <button
                          onClick={context.updateRclHandler.bind(context)}
                          type="submit"
                          className="btn btn-primary btn-block"
                          data-dismiss="modal"
                        >
                          Сохранить
                        </button>
                      </div>
                    </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
