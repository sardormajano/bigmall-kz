import React, {Component} from 'react';

import TopBar from '../stateless/TopBar';
import SearchNav from '../stateless/SearchNav';
import Header from '../stateless/Header';
import PageHeader from '../stateless/PageHeader';
import Footer from '../stateless/Footer';
import Copyright from '../stateless/Copyright';

import {idToLink} from '../../lib/coreLib';

export default class Anonym extends Component {

  getMerchandiseJSX(merchandiseArray, merchandisePhotos, context) {
    return merchandiseArray.map((merchandiseItem, index) => {
      const id = merchandiseItem.photos[0].uniqueIdentifier,
            src = idToLink(id, merchandisePhotos, 'merchandisePhotos');

      return (
        <div key={index} className="col-md-3 col-sm-4 col-xs-12">
          <div className="productBox">
            <div className="productImage clearfix">
              <img src={src} alt="products-img" />
              <div className="productMasking">
                <ul className="list-inline btn-group" role="group">
                  <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                  <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                  <li>
                    <a
                      className="btn btn-default"
                      data-toggle="modal"
                      data-target=".quick-view"
                      onClick={(e) => {context.setState({currentMerchandiseItem: merchandiseItem})}}
                    >
                      <i className="fa fa-eye" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="productCaption clearfix">
              <a href="single-product.html">
                <h5>{merchandiseItem.name}</h5>
              </a>
              <h3>{merchandiseItem.price} тг.</h3>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    const {context, merchandiseFiltered} = this.props,
          {merchandisePhotos, subCategories, user} = context.props;

    return (
      <div>
        {/* HEADER */}
        <div className="header clearfix">
          {/* TOPBAR */}
          <TopBar user={user}/>
          {/* NAVBAR */}
          <SearchNav context={context}/>
        </div>
        {/* LIGHT SECTION */}
        <PageHeader
          pageName="Страница поиска товаров"
          breadcrumbName="Поиск товаров"
        />
        {/* MAIN CONTENT SECTION */}
        <section className="mainContent clearfix productsContent">
          <div className="container">
            <div className="row">
              {this.getMerchandiseJSX(merchandiseFiltered, merchandisePhotos, context)}
            </div>
          </div>
        </section>
        {/* FOOTER */}
        <Footer />
        {/* COPY RIGHT */}
        <Copyright />
        {/* PORDUCT QUICK VIEW MODAL */}
        <div className="modal fade quick-view" tabIndex={-1} role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-body">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div className="media">
                  <div className="media-left">
                    <img className="media-object" src="/img/products/quick-view/quick-view-01.jpg" alt="Image" />
                  </div>
                  <div className="media-body">
                    <h2>Old Skool Navy</h2>
                    <h3>$149</h3>
                    <p>Classic sneaker from Vans. Cotton canvas and suede upper. Textile lining with heel stabilizer and ankle support. Contrasting laces. Sits on a classic waffle rubber sole.</p>
                    <span className="quick-drop">
                      <select name="guiest_id3" id="guiest_id3" className="select-drop">
                        <option value={0}>Size</option>
                        <option value={1}>Size 1</option>
                        <option value={2}>Size 2</option>
                        <option value={3}>Size 3</option>
                      </select>
                    </span>
                    <span className="quick-drop resizeWidth">
                      <select name="guiest_id4" id="guiest_id4" className="select-drop">
                        <option value={0}>Qty</option>
                        <option value={1}>Qty 1</option>
                        <option value={2}>Qty 2</option>
                        <option value={3}>Qty 3</option>
                      </select>
                    </span>
                    <div className="btn-area">
                      <a href="#" className="btn btn-primary btn-block">Add to cart <i className="fa fa-angle-right" aria-hidden="true" /></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
