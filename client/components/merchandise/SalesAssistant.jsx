import React, {Component} from 'react';

import Header from '../stateless/Header';
import PageHeader from '../stateless/PageHeader';
import Footer from '../stateless/Footer';
import Copyright from '../stateless/Copyright';
import Navigation from '../stateless/Navigation';

import {qs} from '../../lib/coreLib';

import Select2 from '../stateless/Select2';

export default class SalesAssistant extends Component {
  componentDidMount() {
    const {merchandisePhotos, context} = this.props;

    merchandisePhotos.resumable.assignBrowse($("#merchandise-photo-input"));

    merchandisePhotos.resumable.on('fileAdded', (file) => {
      const photos = context.state.photos,
            filteredPhotos = photos.filter((photo) => photo.fileName === file.fileName);

      if(filteredPhotos.length)
        return ;

      photos.push({
        fileName: file.fileName,
        uniqueIdentifier: file.uniqueIdentifier
      });
      context.setState({photos});

      merchandisePhotos.insert({
        _id: file.uniqueIdentifier,  // This is the ID resumable will use
        filename: file.fileName,
        contentType: file.file.type
      },
      function (err, _id) {  // Callback to .insert
        if (err) { return console.error("File creation failed!", err); }
        // Once the file exists on the server, start uploading
        merchandisePhotos.resumable.upload();
      }
      );
    });
  }

  getPhotosJSX(photosArray) {
    const {removePhotoHandler} = this.props;

    return photosArray.map((photo, index) => {
      return (
        <div
          key={index}
          className="row"
          style={{marginTop: '0.5em'}}
        >
          <div className="col-sm-9">
            <input
              type="text"
              disabled
              className="form-control"
              value={photo.fileName}
            />
          </div>
          <div className="col-sm-3">
            <a
              onClick={removePhotoHandler}
              data-file-name={photo.fileName}
              href="#"
              className="btn btn-info btn-block col-sm-3"
              role="button"
            >
              удалить
            </a>
          </div>
        </div>
      );
    });
  }

  getMerchandiseJSX(merchandiseArray) {
    const {removeMerchandiseHandler, editMerchandiseModalWillShow} = this.props;

    return merchandiseArray.map((singleMerchandise, index) => {
      return (
        <tr key={index}>
          <td>{singleMerchandise._id}</td>
          <td>{singleMerchandise.name}</td>
          <td>{singleMerchandise.article}</td>
          <td>
            <a
              data-id={singleMerchandise._id}
              data-toggle='modal'
              data-target='#edit-single-merchandise-modal'
              onClick={editMerchandiseModalWillShow}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-edit'></i>
            </a>&nbsp;&nbsp;
            <a
              data-id={singleMerchandise._id}
              onClick={removeMerchandiseHandler}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-remove'></i>
            </a>
          </td>
        </tr>
      );
    });
  }

  render() {
    const {
      user, logoutHandler, avatar, changePhotoHandler,
      changeFileHandler,
      createMerchandiseHandler,
      context,
      categories, subCategories, brands, merchandise, stores
    } = this.props;

    let {hostname} = window.location;
    hostname = hostname == 'localhost' ? 'localhost:3000' : hostname;

    return (
      <div>
        <Header />
        <PageHeader
          pageName='Управление товарами'
          breadcrumbName='Товары'
        />
        <section className="mainContent clearfix userProfile">
          <div className="container">
            <Navigation activeLink='merchandise' />
            <div className="row">
              <div className="col-xs-12">
                <div className="innerWrapper profile">
                  <div className="orderBox">
                    <h4>Управление товарами</h4>
                  </div>
                  <div className="row">
                    <div className="col-md-2 col-sm-3 col-xs-12">
                      <div className="thumbnail">
                        <img
                          src={avatar ? `http://${hostname}/gridfs/userPhotos/${avatar.md5}` : "img/user.png"}
                          alt="profile-image" />
                        <div className="caption">
                          <a onClick={changePhotoHandler} href="#" className="btn btn-primary btn-block" role="button">Сменить Фото</a>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-10 col-sm-9 col-xs-12">
                      <div className="innerWrapper profile">
                        <form className="form-horizontal">
                          <div className="form-group">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Название</label>
                            <div className="col-md-10 col-sm-9">
                              <input
                                value={context.state.name}
                                onChange={(e) => {context.setState({name: e.currentTarget.value});}}
                                type="text"
                                className="form-control"
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Категория</label>
                            <div className="col-md-10 col-sm-9">
                              <Select2
                                options={categories}
                                context={context}
                                label='Выберите категорию'
                                statename='category'
                                className="form-control"
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Подкатегория</label>
                            <div className="col-md-10 col-sm-9">
                              <Select2
                                options={subCategories}
                                context={context}
                                label='Выберите подкатегорию'
                                statename='subCategory'
                                className="form-control"
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Брэнд</label>
                            <div className="col-md-10 col-sm-9">
                              <Select2
                                options={brands}
                                context={context}
                                label='Выберите брэнд'
                                statename='brand'
                                className="form-control"
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Артикул</label>
                            <div className="col-md-10 col-sm-9">
                              <input
                                value={context.state.article}
                                onChange={(e) => {context.setState({article: e.currentTarget.value});}}
                                type="text"
                                className="form-control"
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Доп. характеристики</label>
                            <div className="col-md-10 col-sm-9">
                              <textarea
                                value={context.state.extraFeatures}
                                onChange={(e) => {context.setState({extraFeatures: e.currentTarget.value});}}
                                type="text"
                                className="form-control"
                                style={{height: '10em'}}
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Магазин</label>
                            <div className="col-md-10 col-sm-9">
                              <Select2
                                options={stores}
                                context={context}
                                label='Выберите магазин'
                                statename='store'
                                className="form-control"
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Кол-во в магазине</label>
                            <div className="col-md-10 col-sm-9">
                              <input
                                value={context.state.numInStore}
                                onChange={(e) => {context.setState({numInStore: e.currentTarget.value});}}
                                type="text"
                                className="form-control"
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Цена (тг.)</label>
                            <div className="col-md-10 col-sm-9">
                              <input
                                value={context.state.price}
                                onChange={(e) => {context.setState({price: e.currentTarget.value});}}
                                type="text"
                                className="form-control"
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Скидка (%)</label>
                            <div className="col-md-10 col-sm-9">
                              <input
                                value={context.state.discount}
                                onChange={(e) => {context.setState({discount: e.currentTarget.value});}}
                                type="text"
                                className="form-control"
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Фото</label>
                            <div className="col-md-10 col-sm-9">
                              <a
                                onClick={
                                  (e) => {
                                    e.preventDefault();
                                    qs('#merchandise-photo-input').click();
                                  }
                                }
                                href="#"
                                className="btn btn-primary btn-block"
                                role="button"
                              >
                                Загрузить Фото
                              </a>
                            </div>
                            <div className="hidden">
                              <input
                                id="merchandise-photo-input"
                                type="file"
                                className="form-control"
                                multiple
                              />
                            </div>
                          </div>
                          <div className="form-group">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Список фото</label>
                            <div className="col-md-10 col-sm-9">
                              {this.getPhotosJSX(context.state.photos)}
                            </div>
                          </div>
                          <div className="form-group">
                            <div className="col-md-offset-10 col-md-2 col-sm-offset-9 col-sm-3">
                              <button
                                onClick={createMerchandiseHandler}
                                type="submit"
                                className="btn btn-primary btn-block"
                              >
                                Создать
                              </button>
                            </div>
                          </div>
                          <div className="form-group hidden">
                            <label htmlFor className="col-md-2 col-sm-3 control-label">Фото</label>
                            <div className="col-md-10 col-sm-9">
                              <input
                                id="input-photo"
                                type="file"
                                onChange={changeFileHandler}
                                className="form-control"
                              />
                            </div>
                          </div>
                        </form>
                      </div>
                      <div className="innerWrapper profile">
                        <div className="orderBox">
                          <div className="table-responsive">
                            <table className="table">
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>Название</th>
                                  <th>Артикул</th>
                                  <th>Операции</th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.getMerchandiseJSX(merchandise)}
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Copyright />
      </div>
    );
  }
}
