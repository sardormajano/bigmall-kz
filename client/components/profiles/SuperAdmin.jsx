import React, {Component} from 'react';

import Header from '../stateless/Header';
import PageHeader from '../stateless/PageHeader';
import Footer from '../stateless/Footer';
import Copyright from '../stateless/Copyright';
import Navigation from '../stateless/Navigation';
import Select2 from '../stateless/Select2';
import Pikaday from '../stateless/Pikaday';

import QRCode from 'qrcode.react';

import SuperAdminHelmet from '../../helmets/SuperAdminHelmet';

export default class SuperAdmin extends Component {
  componentDidMount() {
    const {userPhotos} = this.props;

    userPhotos.resumable.assignBrowse($("#input-photo"));

    userPhotos.resumable.on('fileAdded', (file) => {
      Meteor.call('user.changePhoto', (file.uniqueIdentifier), (err) => {
        if(err) {
          Bert.alert( err.reason, 'danger', 'growl-top-right' );
        }
        else {
          Bert.alert( 'Фотография изменена!', 'success', 'growl-top-right' );
        }
      });

      userPhotos.insert({
        _id: file.uniqueIdentifier,  // This is the ID resumable will use
        filename: file.fileName,
        contentType: file.file.type
      },
      function (err, _id) {  // Callback to .insert
        if (err) { return console.error("File creation failed!", err); }
        // Once the file exists on the server, start uploading
        userPhotos.resumable.upload();
      }
      );
    });

  }

  render() {
    const {
      user, logoutHandler, changePhotoHandler, changeFileHandler,
      saveChangesHandler, changePasswordHandler, avatar, dateSelectHandler,
      context, cities, sexes
    } = this.props;
    let {hostname} = window.location,
        profileType;

    hostname = hostname == 'localhost' ? 'localhost:3000' : hostname;

    if(Roles.userIsInRole(user._id, 'a')) {
      profileType = 'суперадмина';
    }
    else if(Roles.userIsInRole(user._id, 'sa')) {
      profileType = 'продавца';
    }
    else {
      profileType = 'покупателя';
    }

    return (
      <div>
        <SuperAdminHelmet />
        <Header />
        <PageHeader
          pageName={`Профиль ${profileType}`}
          breadcrumbName='Профиль'/>
        <section className="mainContent clearfix userProfile">
          <div className="container">
            <Navigation activeLink='profile'/>
            <div className="row">
              <div className="col-xs-12">
                <div className="innerWrapper profile">
                  <div className="orderBox">
                    <h4>Профиль</h4>
                  </div>
                  <div className="row">
                    <div className="col-md-2 col-sm-3 col-xs-12">
                      <div className="thumbnail">
                        <img
                          src={avatar ? `http://${hostname}/gridfs/userPhotos/${avatar.md5}` : "img/user.png"}
                          alt="profile-image" />
                        <div className="caption">
                          <a onClick={changePhotoHandler} href="#" className="btn btn-primary btn-block" role="button">Сменить Фото</a>
                        </div>
                        <br/>
                        <div className="col-sm-12">
                          <QRCode value={`bigmall.kz/${user._id}`}/>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-10 col-sm-9 col-xs-12">
                      <form className="form-horizontal">
                        <div className="form-group">
                          <label htmlFor className="col-md-2 col-sm-3 control-label">Имя</label>
                          <div className="col-md-10 col-sm-9">
                            <input
                              value={context.state.firstName}
                              onChange={(e) => {context.setState({firstName: e.currentTarget.value});}}
                              type="text"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label htmlFor className="col-md-2 col-sm-3 control-label">Фамилия</label>
                          <div className="col-md-10 col-sm-9">
                            <input
                              value={context.state.lastName}
                              onChange={(e) => {context.setState({lastName: e.currentTarget.value});}}
                              type="text"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label htmlFor className="col-md-2 col-sm-3 control-label">Ваш пол</label>
                          <div className="col-md-10 col-sm-9">
                            <Select2
                              options={sexes}
                              defaultValue={context.state.sex}
                              context={context}
                              label='Выберите пол'
                              statename='sex'
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label htmlFor className="col-md-2 col-sm-3 control-label">Ваш город</label>
                          <div className="col-md-10 col-sm-9">
                            <Select2
                              options={cities}
                              defaultValue={context.state.city}
                              context={context}
                              label='Выберите город'
                              statename='city'
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label htmlFor className="col-md-2 col-sm-3 control-label">Дата рождения</label>
                          <div className="col-md-10 col-sm-9">
                            <Pikaday
                              label="Дата рождения"
                              className="form-control"
                              defaultValue={context.state.birthdate}
                              onDateSelect={dateSelectHandler}
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label htmlFor className="col-md-2 col-sm-3 control-label">Телефон</label>
                          <div className="col-md-10 col-sm-9">
                            <input
                              value={context.state.phone}
                              onChange={(e) => {context.setState({phone: e.currentTarget.value});}}
                              type="text"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label htmlFor className="col-md-2 col-sm-3 control-label">Email</label>
                          <div className="col-md-10 col-sm-9">
                            <input
                              value={context.state.email}
                              onChange={(e) => {context.setState({email: e.currentTarget.value});}}
                              type="email"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <div className="col-md-offset-10 col-md-2 col-sm-offset-9 col-sm-3">
                            <button
                              onClick={saveChangesHandler}
                              type="submit"
                              className="btn btn-primary btn-block"
                            >
                              Сохранить
                            </button>
                          </div>
                        </div>
                        <hr/>
                        <div className="form-group">
                          <label htmlFor className="col-md-2 col-sm-3 control-label">Старый пароль</label>
                          <div className="col-md-10 col-sm-9">
                            <input
                              value={context.state.password}
                              onChange={(e) => {context.setState({password: e.currentTarget.value});}}
                              type="password"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <label htmlFor className="col-md-2 col-sm-3 control-label">Новый пароль</label>
                          <div className="col-md-10 col-sm-9">
                            <input
                              value={context.state.newPassword}
                              onChange={(e) => {context.setState({newPassword: e.currentTarget.value});}}
                              type="password"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group hidden">
                          <label htmlFor className="col-md-2 col-sm-3 control-label">Фото</label>
                          <div className="col-md-10 col-sm-9">
                            <input
                              id="input-photo"
                              type="file"
                              onChange={changeFileHandler}
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="form-group">
                          <div className="col-md-offset-10 col-md-2 col-sm-offset-9 col-sm-3">
                            <button
                              onClick={changePasswordHandler}
                              type="submit"
                              className="btn btn-primary btn-block"
                            >
                              Сменить
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Copyright />
      </div>
    );
  }
}
