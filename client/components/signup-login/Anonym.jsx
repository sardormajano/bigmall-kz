import React, {Component} from 'react';

import Header from '../stateless/Header';
import PageHeader from '../stateless/PageHeader';
import Footer from '../stateless/Footer';
import Copyright from '../stateless/Copyright';
import SelectBox from '../stateless/SelectBox';

export default class Anonym extends Component {
  render() {
    const c = this.props.context,
          {signUpHandler, signInHandler, logoutHandler, user} = this.props,
          storeNameJSX = (
            <div className="form-group">
              <label htmlFor>Название магазина</label>
              <input
                onChange={(e) => {c.setState({storeName: e.currentTarget.value});}}
                value={c.state.storeName}
                type="text"
                className="form-control" />
            </div>
          ),
          conditionsJSX = (
            <div className="checkbox">
              <label>
                <input
                  type="checkbox"
                  checked={c.state.agree}
                  onChange={(e) => {c.setState({agree: e.currentTarget.checked});}}/> Согласен с <a className="text-primary" href="terms-and-conditions">условиями пользования.</a>
              </label>
            </div>
          );

    return (
      <div>
        <Header
          user={user}
          logoutHandler={logoutHandler}/>
        <PageHeader
          pageName='Зарегистрироваться/Войти'
          breadcrumbName='Регистрация'/>
        <section className="mainContent clearfix signUp">
          <div className="container">
            <div className="row">
              <div className="col-sm-6 col-xs-12">
                <div className="panel panel-default">
                  <div className="panel-heading"><h3>Создать акаунт</h3></div>
                  <div className="panel-body">
                    <form action method="POST" role="form">
                      <div className="form-group">
                        <SelectBox
                          optionsArray={[
                            {value: 'c', label: 'Я покупатель'},
                            {value: 'sa', label: 'Я продавец'},
                          ]}
                          statename='role'
                          context={c}
                          loginPage={true}
                          />
                      </div>
                      {c.state.role === 'sa' ? storeNameJSX : []}
                      <div className="form-group">
                        <label htmlFor>Имя</label>
                        <input
                          onChange={(e) => {c.setState({firstName: e.currentTarget.value});}}
                          value={c.state.firstName}
                          type="text"
                          className="form-control" />
                      </div>
                      <div className="form-group">
                        <label htmlFor>Фамилия</label>
                        <input
                          onChange={(e) => {c.setState({lastName: e.currentTarget.value});}}
                          value={c.state.lastName}
                          type="text"
                          className="form-control" />
                      </div>
                      <div className="form-group">
                        <label htmlFor>Email</label>
                        <input
                          onChange={(e) => {c.setState({email: e.currentTarget.value});}}
                          value={c.state.email}
                          type="email"
                          className="form-control"/>
                      </div>
                      <div className="form-group">
                        <label htmlFor>Пароль</label>
                        <input
                          onChange={(e) => {c.setState({suPassword: e.currentTarget.value});}}
                          value={c.state.suPassword}
                          type="password"
                          className="form-control" />
                      </div>
                      {c.state.role === 'sa' ? conditionsJSX : []}
                      <button disabled={c.state.role === 'sa' && !c.state.agree} onClick={signUpHandler} type="submit" className="btn btn-primary btn-block">Отправить</button>
                      <button type="button" className="btn btn-link btn-block"><span>Уже зарегистрированы?</span> Войти</button>
                    </form>
                  </div>
                </div>
              </div>
              <div className="col-sm-6 col-xs-12">
                <div className="panel panel-default">
                  <div className="panel-heading"><h3>Уже зарегистрированы?</h3></div>
                  <div className="panel-body">
                    <form action method="POST" role="form">
                      <div className="form-group">
                        <label htmlFor>Логин</label>
                        <input
                          onChange={(e) => {c.setState({siUsername: e.currentTarget.value});}}
                          value={c.state.siUsername}
                          type="text"
                          className="form-control" />
                      </div>
                      <div className="form-group">
                        <label htmlFor>Пароль</label>
                        <input
                          onChange={(e) => {c.setState({siPassword: e.currentTarget.value});}}
                          value={c.state.siPassword}
                          type="password"
                          className="form-control" />
                      </div>
                      <div className="checkbox">
                        <label>
                          <input type="checkbox" /> Запомнить меня
                        </label>
                      </div>
                      <button onClick={signInHandler} type="submit" className="btn btn-primary btn-block">Войти</button>
                      <button type="button" className="btn btn-link btn-block">Не помню пароль</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Copyright />
      </div>
    );
  }
}
