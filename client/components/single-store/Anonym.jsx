import React, {Component} from 'react';

import Header from '../stateless/Header';
import PageHeader from '../stateless/PageHeader';
import Footer from '../stateless/Footer';
import Copyright from '../stateless/Copyright';

import {idToLink, idToName, isSubscribed} from '../../lib/coreLib';

export default class Anonym extends Component {

  getCategoriesJSX(categoriesArray, subCategoriesArray) {
    return categoriesArray.map((category, index) => {
      const currentSubCategories = subCategoriesArray.filter((sc) => sc.category === category._id),
            subCategoriesJSX = currentSubCategories.map((sc, index) => {
              return (
                <li key={index}><a href="#"><i className="fa fa-caret-right" aria-hidden="true" />{sc.name} <span></span></a></li>
              );
            })

      return (
        <li key={index}>
          <a
            href="javascript:;"
            data-toggle="collapse"
            data-target={`#c-${category._id}`}
          >
            {category.name} <i className="fa fa-plus" />
          </a>
          <ul id={`c-${category._id}`} className="collapse collapseItem">
            {subCategoriesJSX}
          </ul>
        </li>
      );
    });
  }

  getMerchandiseJSX(merchandiseArray, merchandisePhotos, context) {
    return merchandiseArray.map((merchandiseItem, index) => {
      const id = merchandiseItem.photos[0].uniqueIdentifier,
            src = idToLink(id, merchandisePhotos, 'merchandisePhotos');

      return (
        <div key={index} className="col-sm-4 col-xs-12">
          <div className="productBox">
            <div className="productImage clearfix">
              <img src={src} alt="products-img" />
              <div className="productMasking">
                <ul className="list-inline btn-group" role="group">
                  <li><a data-toggle="modal" href=".login-modal" className="btn btn-default"><i className="fa fa-heart" /></a></li>
                  <li><a href="cart-page.html" className="btn btn-default"><i className="fa fa-shopping-cart" /></a></li>
                  <li>
                    <a
                      className="btn btn-default"
                      data-toggle="modal"
                      data-target=".quick-view"
                      onClick={(e) => {context.setState({currentMerchandiseItem: merchandiseItem})}}
                    >
                      <i className="fa fa-eye" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="productCaption clearfix">
              <a href="single-product.html">
                <h5>{merchandiseItem.name}</h5>
              </a>
              <h3>{merchandiseItem.price} тг.</h3>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    const {
      currentStore, categories, subCategories, merchandise,
      merchandisePhotos, context
    } = this.props;

    const {currentMerchandiseItem} = context.state;
    let subscribeButton;

    if(isSubscribed(context.props.user, context.props.params.storeId)) {
      subscribeButton = (
        <a onClick={context.unsubscribeHandler.bind(context)} style={{color: 'darkGrey'}} href="#">
          <i className="fa fa-minus-square" /> Отписаться
        </a>
      );
    }
    else {
      subscribeButton = (
        <a onClick={context.subscribeHandler.bind(context)} style={{color: 'darkGrey'}} href="#">
          <i className="fa fa-plus-square" /> Подписаться
        </a>
      );
    }

    return (
      <div>
        <Header />
        <PageHeader
          pageName='Просмотр магазина'
          breadcrumbName='Магазин'
        />
        {/* MAIN CONTENT SECTION */}
        <section className="mainContent clearfix productsContent">
          <div className="container">
            <div className="row">
              <div className="col-md-3 col-sm-4 col-xs-12 sideBar">
                <div className="panel panel-default">
                  <div className="panel-heading">Название магазина</div>
                  <div className="panel-body clearfix">
                    <ul className="list-unstyled clearfix">
                      <li style={{fontSize: '1.5em'}}>{currentStore.name}</li>
                      <li><br/></li>
                      <li>
                        {subscribeButton}
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="panel panel-default">
                  <div className="panel-heading">Категории товаров</div>
                  <div className="panel-body">
                    <div className="collapse navbar-collapse navbar-ex1-collapse navbar-side-collapse">
                      <ul className="nav navbar-nav side-nav">
                        {this.getCategoriesJSX(categories, subCategories)}
                      </ul>
                    </div>
                  </div>
                </div>
                <div className="panel panel-default priceRange">
                  <div className="panel-heading">Фильтровать по цене</div>
                  <div className="panel-body clearfix">
                    <div className="price-slider-inner">
                      <span className="amount-wrapper">
                        Цена:
                        <input type="text" id="price-amount-1" />
                        <strong>-</strong>
                        <input type="text" id="price-amount-2" />
                      </span>
                      <div id="price-range" />
                    </div>
                    <input className="btn btn-default" type="submit" defaultValue="Фильтр" />
                    {/* <span class="priceLabel">Price: <strong>$12 - $30</strong></span> */}
                  </div>
                </div>
              </div>
              <div className="col-md-9 col-sm-8 col-xs-12">
                <div className="row filterArea">
                  <div className="col-xs-6">
                    <select name="guiest_id1" id="guiest_id1" className="select-drop">
                      <option value={0}>Сортировать по умолчанию</option>
                      <option value={1}>Сортировать по популярности</option>
                      <option value={2}>Сортировать по цене</option>
                      <option value={3}>Сортировать по дате</option>
                    </select>
                  </div>
                </div>
                <div className="row">
                  {this.getMerchandiseJSX(merchandise, merchandisePhotos, context)}
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Copyright />
        <div className="modal fade quick-view" tabIndex={-1} role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-body">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div className="media">
                  <div className="media-left" style={{width: '460px'}}>
                    <img
                      className="media-object"
                      src={currentMerchandiseItem ? idToLink(currentMerchandiseItem.photos[0].uniqueIdentifier, merchandisePhotos, 'merchandisePhotos') : ''}
                      alt="Image"
                    />
                  </div>
                  <div className="media-body">
                    <h2>{currentMerchandiseItem ? currentMerchandiseItem.name : ''}</h2>
                    <h3>{currentMerchandiseItem ? currentMerchandiseItem.price : ''} тг.</h3>
                    <p>
                      {currentMerchandiseItem ? currentMerchandiseItem.extraFeatures : ''}
                    </p>
                    <div className="btn-area">
                      <a href="#" className="btn btn-primary btn-block">В корзину <i className="fa fa-angle-right" aria-hidden="true" /></a>
                    </div>
                    <p>
                      <br/><br/>
                      <b>Артикул товара:</b> {currentMerchandiseItem ? currentMerchandiseItem.article : ''}<br/>
                      <b>В наличии:</b> {currentMerchandiseItem ? currentMerchandiseItem.numInStore : ''}<br/>
                      <b>Категория:</b> {currentMerchandiseItem ? idToName(currentMerchandiseItem.category, categories) : ''}
                      , {currentMerchandiseItem ? idToName(currentMerchandiseItem.subCategory, subCategories) : ''}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
