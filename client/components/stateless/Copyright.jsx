import React, {Component} from 'react';

export default class Copyright extends Component {
  render() {
    return (
      <div className="copyRight clearfix">
        <div className="container">
          <div className="row">
            <div className="col-sm-7 col-xs-12">
              <p>© 2016 BigMall все права защищены, автор <a target="_blank" href="#">Сардорбек Мамажанов</a>.</p>
            </div>
            <div className="col-sm-5 col-xs-12">
              <ul className="list-inline">
                <li><a href="#"><i className="fa fa-twitter" /></a></li>
                <li><a href="#"><i className="fa fa-facebook" /></a></li>
                <li><a href="#"><i className="fa fa-dribbble" /></a></li>
                <li><a href="#"><i className="fa fa-vimeo" /></a></li>
                <li><a href="#"><i className="fa fa-tumblr" /></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
