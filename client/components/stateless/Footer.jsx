import React, {Component} from 'react';

export default class Footer extends Component {
  render() {
    return (
      <div className="footer clearfix">
        <div className="container">
          <div className="row">
            <div className="col-sm-2 col-xs-12">
              <div className="footerLink">
                <h5>Категории</h5>
                <ul className="list-unstyled">
                  <li><a href="#">Одежда</a></li>
                  <li><a href="#">Обувь</a></li>
                  <li><a href="#">Аксесуары</a></li>
                  <li><a href="#">Спорт</a></li>
                  <li><a href="#">Красота</a></li>
                </ul>
              </div>
            </div>
            <div className="col-sm-2 col-xs-12">
              <div className="footerLink">
                <h5>Брэнды</h5>
                <ul className="list-unstyled">
                  <li><a href="#">Adidas </a></li>
                  <li><a href="#">Lacoste </a></li>
                  <li><a href="#">Mango</a></li>
                  <li><a href="#">Nike</a></li>
                </ul>
              </div>
            </div>
            <div className="col-sm-2 col-xs-12">
              <div className="footerLink">
                <h5>Новинки</h5>
                <ul className="list-unstyled">
                  <li><a href="#">Одежда</a></li>
                  <li><a href="#">Обувь</a></li>
                  <li><a href="#">Аксесуары</a></li>
                </ul>
              </div>
            </div>
            <div className="col-sm-2 col-xs-12">
              <div className="footerLink">
                <h5>Свяжитесь с нами</h5>
                <ul className="list-unstyled">
                  <li>Позвоните нам по номеру (777)-352-4928</li>
                  <li><a href="mailto:support@bigmall.kz">support@bigmall.kz</a></li>
                </ul>
              </div>
            </div>
            <div className="col-sm-4 col-xs-12">
              <div className="newsletter clearfix">
                <h4>Подписка</h4>
                <h3>Подпишитесь на наши новости!</h3>
                <p>Введите свой email и получайте новости по самым последним новинкам!</p>
                <div className="input-group">
                  <input type="text" className="form-control" placeholder="Ваш email" aria-describedby="basic-addon2" />
                  <a href="#" className="input-group-addon" id="basic-addon2">Ввод <i className="glyphicon glyphicon-chevron-right" /></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
