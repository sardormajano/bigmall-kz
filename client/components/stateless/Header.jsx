import React, {Component} from 'react';

import {Link} from 'react-router';

import TopBar from './TopBar';

export default class Header extends Component {
  componentDidMount() {
    if($(".navbar").width() > 1007)
  	{
  		$('.nav .dropdown').hover(function() {
          	$(this).addClass('open');
  	    },
  	    function() {
  	        $(this).removeClass('open');
  	    });
  	};
  	$('.nav-category .dropdown-submenu ').hover(function() {
      	$(this).addClass('open');
      },
      function() {
          $(this).removeClass('open');
    });
  }

  logoutHandler(e) {
    e.preventDefault();
    Meteor.logout((err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'До свидания!', 'success', 'growl-top-right' );
      }
    });
  }

  render() {
    const user = Meteor.user(),
          profileMenu = user === null ? [] : (
            <li className="dropdown">
              <Link to="/profile-super-admin" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Мой профиль</Link>
              <ul className="dropdown-menu dropdown-menu-right">
                <li><Link to="/profile-super-admin">Личная страница</Link></li>
                <li><Link to="/dictionary-super-admin">Словари</Link></li>
                <li><Link to="/merchandise-sales-assistant">Товары</Link></li>
                <li><Link to="/stores-sales-assistant">Магазины</Link></li>
                <li><a href="account-profile.html">Сообщения</a></li>
                <li><a href="account-all-orders.html">Мои заказы</a></li>
                <li><a href="account-wishlist.html">Мои закладки</a></li>
                <li><a onClick={this.logoutHandler.bind(this)} href="#">Выйти</a></li>
              </ul>
            </li>
          );

    return (
      <div className="header clearfix">
        <TopBar user={user}/>
        <nav className="navbar navbar-main navbar-default" role="navigation">
          <div className="container">
            {/* Brand and toggle get grouped for better mobile display */}
            <div className="navbar-header">
              <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </button>
              <a className="navbar-brand" href="/"><img src="/img/logo.png" alt="logo" /></a>
            </div>
            {/* Collect the nav links, forms, and other content for toggling */}
            <div className="collapse navbar-collapse navbar-ex1-collapse">
              <ul className="nav navbar-nav navbar-right">
                <li>
                  <a role="button">Главная</a>
                </li>
                <li className="dropdown megaDropMenu">
                  <a href="javascript:void(0)" className="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay={300} data-close-others="true" aria-expanded="false">Товары</a>
                  <ul className="dropdown-menu row">
                    <li className="col-sm-3 col-xs-12">
                      <ul className="list-unstyled">
                        <li>Категории</li>
                        <li><a href="#">Одежда</a></li>
                        <li><a href="#">Обувь</a></li>
                        <li><a href="#">Аксесуары</a></li>
                        <li><a href="#">Спорт</a></li>
                        <li><a href="#">Красота</a></li>
                      </ul>
                    </li>
                    <li className="col-sm-3 col-xs-12">
                      <a href="#" className="menu-photo"><img src="/img/menu-photo.jpg" alt="menu-img" /></a>
                    </li>
                  </ul>
                </li>
                {profileMenu}
              </ul>
            </div>{/* /.navbar-collapse */}
          </div>
        </nav>
      </div>
    );
  }
}
