import React, {Component} from 'react';
import {Link} from 'react-router';

export default class Navigation extends Component{
  render() {
    const {activeLink} = this.props;

    const userId = Meteor.userId(),
          navItems = [];

    navItems.push((
      <Link
        key={navItems.length}
        to="/profile-super-admin"
        className={`btn btn-default ${activeLink === 'profile' ? 'active' : ''}`}
      >
        <i className="fa fa-user" aria-hidden="true" />Профиль
      </Link>
    ));

    if(Roles.userIsInRole(userId, 'a')) {
      navItems.push((
        <Link
          key={navItems.length}
          to="/landing-super-admin"
          className={`btn btn-default ${activeLink === 'landing' ? 'active' : ''}`}
        >
          <i className="fa fa-list-alt" aria-hidden="true" />Лэндинг
        </Link>
      ));

      navItems.push((
        <Link
          key={navItems.length}
          to="/dictionary-super-admin"
          className={`btn btn-default ${activeLink === 'dictionaries' ? 'active' : ''}`}
        >
          <i className="fa fa-list" aria-hidden="true" />Словари
        </Link>
      ));
    }
    else if(Roles.userIsInRole(userId, 'sa')) {
      navItems.push((
        <Link
          key={navItems.length}
          to="/stores-sales-assistant"
          className={`btn btn-default ${activeLink === 'stores' ? 'active' : ''}`}
        >
          <i className="fa fa-shopping-bag" aria-hidden="true" />Магазины
        </Link>
      ));

      navItems.push((
        <Link
          key={navItems.length}
          to="/merchandise-sales-assistant"
          className={`btn btn-default ${activeLink === 'merchandise' ? 'active' : ''}`}
        >
          <i className="fa fa-shopping-cart" aria-hidden="true" />Товары
        </Link>
      ));
    }
    else if(Roles.userIsInRole(userId, 'c')) {
      navItems.push((
        <Link
          key={navItems.length}
          to="/subscriptions-customer"
          className={`btn btn-default ${activeLink === 'stores' ? 'active' : ''}`}
        >
          <i className="fa fa-pencil-square" aria-hidden="true" />Подписки
        </Link>
      ));
    }

    return (
      <div className="row">
        <div className="col-xs-12">
          <div className="btn-group" role="group" aria-label="...">
            {navItems}
          </div>
        </div>
      </div>
    );
  }
}
