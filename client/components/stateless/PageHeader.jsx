import React, {Component} from 'react';

import {Link} from 'react-router';

export default class PageHeader extends Component {
  render() {
    return (
      <section className="lightSection clearfix pageHeader">
        <div className="container">
          <div className="row">
            <div className="col-xs-6">
              <div className="page-title">
                <h2>{this.props.pageName}</h2>
              </div>
            </div>
            <div className="col-xs-6">
              <ol className="breadcrumb pull-right">
                <li>
                  <Link to="/">Главная</Link>
                </li>
                <li className="active">{this.props.breadcrumbName}</li>
              </ol>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
