import React, {Component} from 'react';

import {qs} from '../../lib/coreLib';
import moment from '../../lib/moment-with-locales.min.js';

export default class Pickaday extends Component {
  setPikaday() {
    $.getScript('/plugins/pikaday/pikaday.js', () => {
      const ru = {
          previousMonth : 'Пред. месяц',
          nextMonth     : 'След. месяц',
          months        : ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
          weekdays      : ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
          weekdaysShort : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб']
      };

      const picker = new Pikaday({
        field: this.pikaday,
        i18n: ru,
        defaultDate: new Date(this.props.defaultValue),
        firstDay: 1,
        yearRange: [70],
        minDate: new Date(1960, 1, 1),
        maxDate: new Date(),
        onSelect: this.props.onDateSelect.bind(this.props.context, this.pikaday)
      });
    });
  }

  componentDidMount() {
    this.setPikaday();
  }

  render() {
    const {label, className, defaultValue} = this.props;

    moment.locale('ru');
    const formattedDate = defaultValue ? moment(defaultValue).format('LL') : '';

    return (
      <input type="text"
        id='pikaday-input'
        ref={(pikaday) => {this.pikaday = pikaday;}}
        className={className}
        placeholder={label}
        value={formattedDate}
      />
    );
  }
}
