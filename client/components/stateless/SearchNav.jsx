import React, {Component} from 'react';
import SelectBox from './SelectBox';

import {Link} from 'react-router';

export default class SearchNav extends Component {
  render() {
    const {context} = this.props,
          {subCategories} = context.props;

    const optionsArray = subCategories.map((item) => {
      return {
        value: item._id,
        label: item.name
      }
    });

    optionsArray.unshift({
      value: 0,
      label: 'Все категории'
    });

    return (
      <div className="navTop text-center">
        <div className="container">
          <div className="navbar-header">
            <Link className="navbar-brand" to="/"><img src="/img/logo.png" alt="logo+" /></Link>
          </div>
          <div className="navTop-middle">
            <div className="filterArea hidden-xs hidden-sm">
              <SelectBox
                context={context}
                statename='searchCategory'
                optionsArray={optionsArray}
              />
            </div>
            <div className="searchBox">
              <span className="input-group">
                <input
                  value={context.state.searchText}
                  onChange={(e) => {context.setState({searchText: e.currentTarget.value})}}
                  type="text"
                  className="form-control"
                  placeholder="Поиск товаров…"
                  aria-describedby="basic-addon2"
                />
                <button
                  onClick={context.searchButtonHandler.bind(context)}
                  type="submit"
                  className="input-group-addon"
                >
                  <i className="fa fa-search" />
                </button>
              </span>
            </div>
          </div>
          <div className="dropdown cart-dropdown">
            <a href="cart-page.html" className="dropdown-toggle shop-cart" data-toggle="dropdown">
              <i className="fa fa-shopping-cart" />
              <span className="hidden-xs">
                <span className="cart-total">Ваша корзина (3)</span><br />
                <span className="cart-price">4350 тг.</span>
              </span>
            </a>
            <ul className="dropdown-menu dropdown-menu-right">
              <li>Товары в вашей корзине</li>
              <li>
                <a href="single-product.html">
                  <div className="media">
                    <img className="media-left media-object" src="/img/home/cart-items/cart-item-01.jpg" alt="cart-Image" />
                    <div className="media-body">
                      <h5 className="media-heading">Брюки <br /><span>1 X 4350 тг.</span></h5>
                    </div>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
