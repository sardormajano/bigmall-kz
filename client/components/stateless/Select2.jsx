import React, {Component} from 'react';

export default class Select2 extends Component {

  componentDidMount() {
    $.getScript('/plugins/select2/js/select2.full.min.js', () => {
      const {context, statename} = this.props;

      $(this.sel)
      .select2({minimumResultsForSearch: 7})
      .on('change', (e) => {
        context.setState({[statename]: e.currentTarget.value})
      });

      $('.select2.select2-container').addClass('form-control');
    });
  }

  componentDidUpdate() {
    const {context, statename} = this.props;

    $.getScript('/plugins/select2/js/select2.full.min.js', () => {
      const {context, statename} = this.props;

      $(this.sel).siblings().filter('.select2.select2-container.select2-container--default.select2-container--focus').remove();

      $(this.sel)
      .select2({minimumResultsForSearch: 7})
      .off();

      $(this.sel)
      .select2({minimumResultsForSearch: 7})
      .on('change', (e) => {
        context.setState({[statename]: e.currentTarget.value})
      });
    });
  }

  getOptionsJSX(options) {
    return options.map((option, defaultValue) => {
      return (
        <option
          key={option._id}
          value={option._id}
        >
          {option.name}
        </option>
      );
    });
  }

  render() {
    const {
      options, defaultValue, context, statename,
      className, label
    } = this.props;

    return (
      <select
        style={{width: '100%'}}
        className={className}
        ref={(sel) => {this.sel = sel;}}
        value={context.state[statename]}
        onChange={(e) => {context.setState({[statename]: e.currentTarget.value})}}
      >
        <option value="0" disabled>{label}</option>
        {this.getOptionsJSX(options)}
      </select>
    );
  }
}
