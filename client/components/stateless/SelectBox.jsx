import React, {Component} from 'react';

export default class SelectBox extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const {statename, context} = this.props;

    $.getScript('/plugins/selectbox/jquery.selectbox-0.1.3.min.js', () => {
      const selectBox = $('.select-drop')
      .selectbox({menuSpeed: 'fast'});

      $('.sbHolder .sbOptions li a').click((e) => {
        context.setState({[statename]: e.currentTarget.getAttribute('rel')});
      });
    });
  }

  arrayToOptions(optionsArray) {
    return optionsArray.map((option, index) => {
      return <option key={index} value={option.value}>{option.label || option.value}</option>;
    });
  }

  render() {
    const {optionsArray, statename, context, loginPage} = this.props,
          options = this.arrayToOptions(optionsArray);

    return (
      <div className={loginPage ? "dropGeneral colorBg" : ''}>
        <select className="select-drop">
          {options}
        </select>
      </div>
    );
  }
}
