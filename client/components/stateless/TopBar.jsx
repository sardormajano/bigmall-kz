import React, {Component} from 'react';
import {Link} from 'react-router';

export default class TopBar extends Component {
  componentDidMount() {
    $('.searchBox a').on("click",function() {
        $(".searchBox .dropdown-menu").toggleClass('display-block');
        $(".searchBox a i").toggleClass('fa-close').toggleClass("fa-search");
    });
  }

  render() {
    const {user} = this.props;

    let userInfo;

    if(user) {
      userInfo = (
        <li className="account-login">
          <span>
            <Link to="/profile-super-admin">
              {`${user.profile.firstName} ${user.profile.lastName}`}
            </Link>
          </span>
        </li>
      );
    }
    else {
      userInfo = (
        <li className="account-login">
          <span>
            <Link to="/signup-login">Войти</Link>
            <small>или</small>
            <Link to="signup-login">Зарегистрироваться</Link>
          </span>
        </li>
      );
    }

    return (
      <div className="topBar">
        <div className="container">
          <div className="row">
            <div className="col-md-6 hidden-sm hidden-xs">
              <ul className="list-inline">
                <li><a href="#"><i className="fa fa-twitter" /></a></li>
                <li><a href="#"><i className="fa fa-facebook" /></a></li>
                <li><a href="#"><i className="fa fa-dribbble" /></a></li>
                <li><a href="#"><i className="fa fa-vimeo" /></a></li>
                <li><a href="#"><i className="fa fa-tumblr" /></a></li>
              </ul>
            </div>
            <div className="col-md-6 col-xs-12">
              <ul className="list-inline pull-right top-right">
                {userInfo}
                <li className="searchBox">
                  <a href="#">
                    <i className="fa fa-search" />
                  </a>
                  <ul className="dropdown-menu dropdown-menu-right">
                    <li>
                      <span className="input-group">
                        <input type="text" className="form-control" placeholder="Искать…" aria-describedby="basic-addon2" />
                        <button type="submit" className="input-group-addon">Поиск</button>
                      </span>
                    </li>
                  </ul>
                </li>
                <li className="dropdown cart-dropdown">
                  <a href="javascript:void(0)" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-shopping-cart" />$0</a>
                  <ul className="dropdown-menu dropdown-menu-right">
                    <li>Ваша корзина</li>
                    <li>
                      <div className="btn-group" role="group" aria-label="...">
                        <button type="button" className="btn btn-default">Моя корзина</button>
                        <button type="button" className="btn btn-default">Оплатить</button>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
