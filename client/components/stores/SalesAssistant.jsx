import React, {Component} from 'react';

import Header from '../stateless/Header';
import PageHeader from '../stateless/PageHeader';
import Footer from '../stateless/Footer';
import Copyright from '../stateless/Copyright';
import Navigation from '../stateless/Navigation';

import Select2 from '../stateless/Select2';

import {Link} from 'react-router';

import {qs} from '../../lib/coreLib';

export default class SalesAssistant extends Component {
  componentDidMount() {
    const {userPhotos, storeLogos, context} = this.props;

    userPhotos.resumable.assignBrowse($("#input-photo"));

    userPhotos.resumable.on('fileAdded', (file) => {
      Meteor.call('user.changePhoto', (file.uniqueIdentifier), (err) => {
        if(err) {
          Bert.alert( err.reason, 'danger', 'growl-top-right' );
        }
        else {
          Bert.alert( 'Фотография изменена!', 'success', 'growl-top-right' );
        }
      });

      userPhotos.insert({
        _id: file.uniqueIdentifier,  // This is the ID resumable will use
        filename: file.fileName,
        contentType: file.file.type
      },
      function (err, _id) {  // Callback to .insert
        if (err) { return console.error("File creation failed!", err); }
        // Once the file exists on the server, start uploading
        userPhotos.resumable.upload();
      }
      );
    });

    storeLogos.resumable.assignBrowse($("#new-logo-input"));

    storeLogos.resumable.on('fileAdded', (file) => {
      context.setState({
        logo: {
          uniqueIdentifier: file.uniqueIdentifier,
          fileName: file.fileName
        }
      });

      storeLogos.insert({
        _id: file.uniqueIdentifier,  // This is the ID resumable will use
        filename: file.fileName,
        contentType: file.file.type
      },
      function (err, _id) {  // Callback to .insert
        if (err) { return console.error("File creation failed!", err); }
        // Once the file exists on the server, start uploading
        storeLogos.resumable.upload();
      }
      );
    });
  }

  getStoresJSX(storesArray) {
    const {
      editStoreModalWillShow,
      createStoreHandler, updateStoreHandler, removeStoreHandler,
      context
    } = this.props;

    return storesArray.map((store, index) => {
      return (
        <tr key={index}>
          <td>{store._id}</td>
            <td><Link style={{color: 'black'}} to={`single-store/${store._id}`}>{store.name}</Link></td>
          <td>
            <a
              data-id={store._id}
              data-toggle='modal'
              data-target='#edit-store-modal'
              onClick={editStoreModalWillShow}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-edit'></i>
            </a>&nbsp;&nbsp;
            <a
              data-id={store._id}
              onClick={removeStoreHandler}
              href="#"
              className="btn btn-default"
            >
              <i className='fa fa-remove'></i>
            </a>
          </td>
        </tr>
      );
    });
  }

  render() {
    const {
      avatar, context, changePhotoHandler,
      stores, removeStoreHandler, updateStoreHandler, createStoreHandler
    } = this.props;

    let {hostname} = window.location;
    hostname = hostname == 'localhost' ? 'localhost:3000' : hostname;

    return (
      <div>
        <Header />
        <PageHeader
          pageName='Управление магазинами'
          breadcrumbName='Магазины'
        />
        <section className="mainContent clearfix userProfile">
          <div className="container">
            <Navigation activeLink='stores' />
            <div className="row">
              <div className="col-xs-12">
                <div className="innerWrapper profile">
                  <div className="orderBox">
                    <h4>Управление магазинами</h4>
                  </div>
                  <div className="row">
                    <div className="col-md-2 col-sm-3 col-xs-12">
                      <div className="thumbnail">
                        <img
                          src={avatar ? `http://${hostname}/gridfs/userPhotos/${avatar.md5}` : "img/user.png"}
                          alt="profile-image" />
                        <div className="caption">
                          <a onClick={changePhotoHandler} href="#" className="btn btn-primary btn-block" role="button">Сменить Фото</a>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-10 col-sm-9 col-xs-12">
                      <div className="innerWrapper">
                        <div className="orderBox">
                          <h4>Управление магазинами</h4>
                          <div className="col-sm-3">
                            <a
                              data-toggle='modal'
                              data-target="#add-store-modal"
                              className="btn btn-info btn-block"
                              role="button"
                            >
                              Добавить
                            </a>
                            <br/><br/>
                          </div>
                          <div className="table-responsive">
                            <table className="table">
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>Название</th>
                                  <th>Операции</th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.getStoresJSX(stores)}
                              </tbody>
                            </table>
                          </div>
                          <br />
                          <div className="col-sm-3">
                            <a
                              data-toggle='modal'
                              data-target="#add-store-modal"
                              className="btn btn-info btn-block"
                              role="button"
                            >
                              Добавить
                            </a>
                          </div>
                          <br />
                          <br />
                        </div>
                        <div id="add-store-modal" className="modal fade" tabIndex={-1} role="dialog">
                          <div className="modal-dialog">
                            <div className="modal-content">
                              <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 className="modal-title" id="myModalLabel">Добавление магазина</h4>
                              </div>
                              <div className="modal-body">
                                  <form className="form-horizontal">
                                    <div className="form-group">
                                      <label htmlFor className="col-md-2 col-sm-3 control-label">Название</label>
                                      <div className="col-md-10 col-sm-9">
                                        <input
                                          value={context.state.storeName}
                                          onChange={(e) => {context.setState({storeName: e.currentTarget.value});}}
                                          type="text"
                                          className="form-control"
                                        />
                                      </div>
                                    </div>
                                    <div className="form-group">
                                      <label htmlFor className="col-md-2 col-sm-3 control-label">Адрес</label>
                                      <div className="col-md-10 col-sm-9">
                                        <input
                                          value={context.state.address}
                                          onChange={(e) => {context.setState({address: e.currentTarget.value});}}
                                          type="text"
                                          className="form-control"
                                        />
                                      </div>
                                    </div>
                                    <div className="form-group">
                                      <label htmlFor className="col-md-2 col-sm-3 control-label">Логотип</label>
                                      <div className="col-md-10 col-sm-9">
                                        <a
                                          onClick={
                                            (e) => {
                                              e.preventDefault();
                                              qs('#new-logo-input').click();
                                            }
                                          }
                                          href="#"
                                          className="btn btn-primary btn-block"
                                          role="button"
                                        >
                                          {context.state.logo.fileName ? context.state.logo.fileName : 'Загрузить Фото'}
                                        </a>
                                      </div>
                                      <div className="hidden">
                                        <input
                                          id="new-logo-input"
                                          type="file"
                                          className="form-control"
                                          multiple
                                        />
                                      </div>
                                    </div>
                                    <div className="form-group">
                                      <label className="col-md-2 col-sm-3 control-label">Доставка</label>
                                      <div className="col-md-10 col-sm-9">
                                        <Select2
                                          options={[
                                            {
                                              name: 'Есть',
                                              _id: true
                                            },
                                            {
                                              name: 'Нет',
                                              _id: false
                                            }
                                          ]}
                                          context={context}
                                          label='Имеется ли доставка'
                                          statename='hasDelivery'
                                          className="form-control"
                                        />
                                      </div>
                                    </div>
                                    <div className="form-group">
                                      <div className="col-sm-4 pull-right">
                                        <button
                                          onClick={createStoreHandler}
                                          type="submit"
                                          className="btn btn-primary btn-block"
                                          data-dismiss="modal"
                                        >
                                          Сохранить
                                        </button>
                                      </div>
                                    </div>
                                  </form>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div id="edit-store-modal" className="modal fade" tabIndex={-1} role="dialog">
                            <div className="modal-dialog">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                                  <h4 className="modal-title" id="myModalLabel">Редактирование магазина</h4>
                                </div>
                                <div className="modal-body">
                                    <form className="form-horizontal">
                                      <div className="form-group">
                                        <label htmlFor className="col-md-2 col-sm-3 control-label">Название</label>
                                        <div className="col-md-10 col-sm-9">
                                          <input
                                            value={context.state.estoreName}
                                            onChange={(e) => {context.setState({estoreName: e.currentTarget.value});}}
                                            type="text"
                                            className="form-control"
                                          />
                                        </div>
                                      </div>
                                      <div className="form-group">
                                        <label htmlFor className="col-md-2 col-sm-3 control-label">Адрес</label>
                                        <div className="col-md-10 col-sm-9">
                                          <input
                                            value={context.state.eaddress}
                                            onChange={(e) => {context.setState({eaddress: e.currentTarget.value});}}
                                            type="text"
                                            className="form-control"
                                          />
                                        </div>
                                      </div>
                                      <div className="form-group">
                                        <label htmlFor className="col-md-2 col-sm-3 control-label">Логотип</label>
                                        <div className="col-md-10 col-sm-9">
                                          <a
                                            onClick={
                                              (e) => {
                                                e.preventDefault();
                                                qs('#edit-logo-input').click();
                                              }
                                            }
                                            href="#"
                                            className="btn btn-primary btn-block"
                                            role="button"
                                          >
                                            {context.state.elogo.fileName ? context.state.elogo.fileName : 'Загрузить Фото'}
                                          </a>
                                        </div>
                                        <div className="hidden">
                                          <input
                                            id="edit-logo-input"
                                            type="file"
                                            className="form-control"
                                            multiple
                                          />
                                        </div>
                                      </div>
                                      <div className="form-group">
                                        <label className="col-md-2 col-sm-3 control-label">Доставка</label>
                                        <div className="col-md-10 col-sm-9">
                                          <Select2
                                            options={[
                                              {
                                                name: 'Есть',
                                                _id: true
                                              },
                                              {
                                                name: 'Нет',
                                                _id: false
                                              }
                                            ]}
                                            context={context}
                                            label='Имеется ли доставка'
                                            statename='ehasDelivery'
                                            className="form-control"
                                          />
                                        </div>
                                      </div>
                                      <div className="form-group">
                                        <div className="col-sm-4 pull-right">
                                          <button
                                            onClick={updateStoreHandler}
                                            type="submit"
                                            className="btn btn-primary btn-block"
                                            data-dismiss="modal"
                                          >
                                            Сохранить
                                          </button>
                                        </div>
                                      </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Copyright />
      </div>
    );
  }
}
