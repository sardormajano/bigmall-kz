import React, {Component} from 'react';

import Header from '../stateless/Header';
import PageHeader from '../stateless/PageHeader';
import Footer from '../stateless/Footer';
import Copyright from '../stateless/Copyright';
import Navigation from '../stateless/Navigation';

export default class Customer extends Component {
  render() {
    const {
      avatar, context, changePhotoHandler,
      stores
    } = this.props;

    let {hostname} = window.location;
    hostname = hostname == 'localhost' ? 'localhost:3000' : hostname;

    return (
      <div>
        <Header />
        <PageHeader
          pageName='Управление подписками'
          breadcrumbName='Подписки'
        />
        <section className="mainContent clearfix userProfile">
          <div className="container">
            <Navigation activeLink='stores' />
            <div className="row">
              <div className="col-xs-12">
                <div className="innerWrapper profile">
                  <div className="orderBox">
                    <h4>Подписки</h4>
                  </div>
                  <div className="row">
                    <div className="col-md-2 col-sm-3 col-xs-12">
                      <div className="thumbnail">
                        <img
                          src={avatar ? `http://${hostname}/gridfs/userPhotos/${avatar.md5}` : "img/user.png"}
                          alt="profile-image" />
                        <div className="caption">
                          <a onClick={changePhotoHandler} href="#" className="btn btn-primary btn-block" role="button">Сменить Фото</a>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-10 col-sm-9 col-xs-12">
                      <div className="innerWrapper">
                        <div className="orderBox">
                          <h4>Мои подписки</h4>
                          <div className="table-responsive">
                            <table className="table">
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>Название</th>
                                  <th>Операции</th>
                                </tr>
                              </thead>
                              <tbody>
                                {/*this.getStoresJSX(stores)*/}
                              </tbody>
                            </table>
                          </div>
                          <br />
                          <br />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Copyright />
      </div>
    );
  }
}
