import React, {Component} from 'react';

import Header from '../stateless/Header';
import PageHeader from '../stateless/PageHeader';
import Footer from '../stateless/Footer';
import Copyright from '../stateless/Copyright';

export default class Anonym extends Component {
  render() {
    const {user, logoutHandler} = this.props;

    return (
      <div>
        <Header
          user={user}
          logoutHandler={logoutHandler}/>
        <PageHeader
          pageName='Условия договора'
          breadcrumbName='Договор'/>
        <section className="mainContent clearfix genricContent">
          <div className="container">
            <div className="orderBox">
              <h4>Условия пакетов</h4>
              <div id="conditions-table" className="table-responsive">
                <table className="table">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Стандартный</th>
                      <th>Золотой</th>
                      <th>Платиновый</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><strong>Страница на портале (личный кабинет)</strong></td>
                      <td>+</td>
                      <td>+</td>
                      <td>+</td>
                    </tr>
                    <tr>
                      <td><strong>Аналитика продаж</strong></td>
                      <td>+</td>
                      <td>+</td>
                      <td>+</td>
                    </tr>
                    <tr>
                      <td><strong>Поддержка Call- center 24/7</strong></td>
                      <td>+</td>
                      <td>+</td>
                      <td>+</td>
                    </tr>
                    <tr>
                      <td><strong>PUSH уведомления</strong></td>
                      <td>до 1 000</td>
                      <td>до 7 000</td>
                      <td>до 15 000</td>
                    </tr>
                    <tr>
                      <td>
                        <strong>
                          Реклама на главной странице (размещение на основном рекламном баннере - 5 дней в месяц)
                        </strong>
                      </td>
                      <td>-</td>
                      <td>-</td>
                      <td>+</td>
                    </tr>
                    <tr>
                      <td>
                        <strong>
                          Реклама на главной странице (в категориях)
                        </strong>
                      </td>
                      <td>-</td>
                      <td>+</td>
                      <td>+</td>
                    </tr>
                    <tr>
                      <td>
                        <strong>
                          Размещение товаров в разделе «Горячие предложения»
                        </strong>
                      </td>
                      <td>-</td>
                      <td>+</td>
                      <td>+</td>
                    </tr>
                    <tr>
                      <td>
                        <strong>
                          Товары размещаются первыми в очереди при Поиске товаров
                        </strong>
                      </td>
                      <td>-</td>
                      <td>-</td>
                      <td>+</td>
                    </tr>
                    <tr className="bg-primary">
                      <td>
                        <strong>
                          СТОИМОСТЬ
                        </strong>
                      </td>
                      <td>
                        <strong>
                          6 900 тенге
                        </strong>
                      </td>
                      <td>
                        <strong>
                          9 900 тенге
                        </strong>
                      </td>
                      <td>
                        <strong>
                          12 900 тенге
                        </strong>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="row">
              <div className="col-xs-12">
                <h3>Предмет Договора</h3>
                <p>Работник принимается на работу на должность Продавца в онлайн маркет "BigMall", расположенном по адресу г. Астана ул. Иле дом 37.</p>
                <p>Настоящий Договор является Договором:<br/><br/>- по основному месту работы.</p>
                <h4>Срок Договора</h4>
                <p>Настоящий Договор заключен на неопределенный срок.</p>
                <p>Работник обязуется приступить к исполнению обязанностей, предусмотренных в п.3.2. параграфа 3 настоящего Договора с 25 июня 2012 года.</p>
                <p>Настоящим Договором устанавливается испытательный срок три месяца с момента начала работы.</p>
                <h4>Права и обязанности Работника</h4>
                <p>
                  Работник имеет право на:
                  <br/><br/>-предоставление ему работы, обусловленной трудовым Договором;
                  <br/>-рабочее место, соответствующее условиям, предусмотренным Государственными стандартами организации и безопасности труда и коллективным Договором;
                  <br/>-полную достоверную информацию об условиях труда и требованиях охраны труда на рабочем месте;
                  <br/>-защиту персональных данных;
                  <br/>-продолжительность рабочего времени в соответствии с действующим законодательством;
                </p>
                <h4>Права и обязанности Работодателя</h4>
                <p>
                  Работодатель имеет право:
                  <br/><br/>-вести коллективные переговоры и заключать коллективные Договоры;
                  <br/>-поощрять Работника за добросовестный эффективный труд;
                  <br/>-требовать от Работника исполнения им трудовых обязанностей и бережного отношения к имуществу Работодателя и других работников, соблюдения правил внутреннего трудового распорядка организации;
                  <br/>-привлекать Работника к дисциплинарной и материальной ответственности в порядке, установленном ТК РФ, иными федеральными законами;
                </p>
              </div>
            </div>
          </div>
        </section>
        <Footer />
        <Copyright />
      </div>
    );
  }
}
