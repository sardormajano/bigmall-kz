import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import {createContainer} from 'meteor/react-meteor-data';

import {qs} from '../../lib/coreLib';

import SuperAdmin from '../../components/dictionaries/SuperAdmin';
import {userPhotos} from '../../../api/Users';
import {UsersCollection} from '../../../api/Users';
import {CitiesCollection} from '../../../api/Cities';
import {SexesCollection} from '../../../api/Sexes';

import {CategoriesCollection} from '../../../api/Categories';
import {SubCategoriesCollection} from '../../../api/SubCategories';
import {BrandsCollection} from '../../../api/Brands';
import {ExtraFeaturesCollection} from '../../../api/ExtraFeatures';

class SuperAdminContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      newPassword: '',

      userBeingEdited: '',

      efirstName: '',
      elastName: '',
      eemail: '',
      enewPassword: '',

      categoryName: '',
      categoryBeingEdited: '',
      ecategoryName: '',

      scategory: '',
      ssubCategoryName: '',
      ssubCategoryBeingEdited: '',
      essubCategoryName: '',
      escategory: '',

      brandName: '',
      brandBeingEdited: '',
      ebrandName: '',

      extraFeatureName: '',
      extraFeatureBeingEdited: '',
      eextraFeatureName: '',

      usersReady: false,
      categoriesReady: false,
      subCategoriesReady: false,
      brandsReady: false,
      extraFeaturesReady: false
    };
  }

  componentWillMount() {
    Meteor.subscribe('photos', (Meteor.userId()));

    Meteor.subscribe('UsersList', {
      onReady: () => {
        this.setState({usersReady: true});
      }
    });

    Meteor.subscribe('Cities', {
      onReady: () => {
        this.setState({
          citiesReady: true
        })
      }
    });

    Meteor.subscribe('Sexes', {
      onReady: () => {
        this.setState({
          sexesReady: true
        })
      }
    });

    Meteor.subscribe('CurrentUser', {
      onReady: () => {
        if(!this.props.user) {
          browserHistory.push('signup-login');
          return ;
        }
      }
    });

    Meteor.subscribe('Categories', {
      onReady: () => {
        this.setState({
          categoriesReady: true
        })
      }
    });

    Meteor.subscribe('SubCategories', {
      onReady: () => {
        this.setState({
          subCategoriesReady: true
        })
      }
    });

    Meteor.subscribe('Brands', {
      onReady: () => {
        this.setState({
          brandsReady: true
        })
      }
    });

    Meteor.subscribe('ExtraFeatures', {
      onReady: () => {
        this.setState({
          extraFeaturesReady: true
        })
      }
    });
  }

  editModalWillShow(e) {
    const userId = e.currentTarget.getAttribute('data-id'),
          currentUser = UsersCollection.findOne({_id: userId});

    this.setState({
      efirstName: currentUser.profile.firstName,
      elastName: currentUser.profile.lastName,
      eemail: currentUser.emails[0].address,
      userBeingEdited: currentUser._id
    });
  }

  editCategoryModalWillShow(e) {
    const categoryId = e.currentTarget.getAttribute('data-id'),
          currentCategory = CategoriesCollection.findOne({_id: categoryId});

    this.setState({
      ecategoryName: currentCategory.name,
      categoryBeingEdited: currentCategory._id
    });
  }

  editSubCategoryModalWillShow(e) {
    const subCategoryId = e.currentTarget.getAttribute('data-id'),
          currentSubCategory = SubCategoriesCollection.findOne({_id: subCategoryId});

    this.setState({
      essubCategoryName: currentSubCategory.name,
      escategory: currentSubCategory.category,
      ssubCategoryBeingEdited: currentSubCategory._id
    });
  }

  editBrandModalWillShow(e) {
    const brandId = e.currentTarget.getAttribute('data-id'),
          currentBrand = BrandsCollection.findOne({_id: brandId});

    this.setState({
      ebrandName: currentBrand.name,
      brandBeingEdited: currentBrand._id
    });
  }

  editExtraFeatureModalWillShow(e) {
    const extraFeatureId = e.currentTarget.getAttribute('data-id'),
          currentExtraFeature = ExtraFeaturesCollection.findOne({_id: extraFeatureId});

    this.setState({
      eextraFeatureName: currentExtraFeature.name,
      extraFeatureBeingEdited: currentExtraFeature._id
    });
  }

  createUserHandler(e) {
    e.preventDefault();


    const userOptions = {
      username: this.state.email,
      password: this.state.newPassword,
      email: this.state.email,
      profile: {
        firstName: this.state.firstName,
        lastName: this.state.lastName
      }
    };

    Meteor.call('user.create', userOptions, 'a', (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Акаунт успешно создан!', 'success', 'growl-top-right' );
        this.setState({
          firstName: '',
          lastName: '',
          email: '',
          newPassword: '',
        });
      }
    });
  }

  updateUserHandler(e) {
    e.preventDefault();

    const userOptions = {
      username: this.state.eemail,
      email: this.state.eemail,
      profile: {
        firstName: this.state.efirstName,
        lastName: this.state.elastName
      }
    };

    Meteor.call('admin.update', this.state.userBeingEdited, userOptions, this.state.enewPassword, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Акаунт успешно изменен!', 'success', 'growl-top-right' );
        this.setState({
          userBeingEdited: '',

          efirstName: '',
          elastName: '',
          eemail: '',
          enewPassword: '',
        });
      }
    });
  }

  removeUserHandler(e) {
    e.preventDefault();

    const currentButton = e.currentTarget,
          userId = currentButton.getAttribute('data-id');

    Meteor.call('user.remove', userId, {'profile.deleted': true}, (err) => {
      if(err) {
        Bert.alert(err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert('Пользователь успешно удален!', 'success', 'growl-top-right' );
      }
    })
  }

  createCategoryHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.categoryName
    };

    Meteor.call('categories.add', data, 'a', (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Категория успешно создана!', 'success', 'growl-top-right' );
        this.setState({
          categoryName: '',
        });
      }
    });
  }

  updateCategoryHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.ecategoryName,
    };

    Meteor.call('categories.edit', this.state.categoryBeingEdited, data, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Категория успешно изменена!', 'success', 'growl-top-right' );
        this.setState({
          ecategoryName: '',
          categoryBeingEdited: ''
        });
      }
    });
  }

  removeCategoryHandler(e) {
    e.preventDefault();

    const currentButton = e.currentTarget,
          categoryId = currentButton.getAttribute('data-id');

    Meteor.call('categories.remove', categoryId, (err) => {
      if(err) {
        Bert.alert(err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert('Категория успешно удалена!', 'success', 'growl-top-right' );
      }
    })
  }

  createSubCategoryHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.ssubCategoryName,
      category: this.state.scategory,
    };

    Meteor.call('subCategories.add', data, 'a', (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Подкатегория успешно создана!', 'success', 'growl-top-right' );
        this.setState({
          scategory: '',
          ssubCategoryName: '',
        });
      }
    });
  }

  updateSubCategoryHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.essubCategoryName,
      category: this.state.escategory,
    };

    Meteor.call('categories.edit', this.state.ssubCategoryBeingEdited, data, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Подкатегория успешно изменена!', 'success', 'growl-top-right' );
        this.setState({
          ssubCategoryBeingEdited: '',
          essubCategoryName: '',
          escategory: '',
        });
      }
    });
  }

  removeSubCategoryHandler(e) {
    e.preventDefault();

    const currentButton = e.currentTarget,
          subCategoryId = currentButton.getAttribute('data-id');

    Meteor.call('subCategories.remove', subCategoryId, (err) => {
      if(err) {
        Bert.alert(err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert('Подкатегория успешно удалена!', 'success', 'growl-top-right' );
      }
    })
  }

  createBrandHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.brandName
    };

    Meteor.call('brands.add', data, 'a', (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Брэнд успешно создан!', 'success', 'growl-top-right' );
        this.setState({
          brandName: '',
        });
      }
    });
  }

  updateBrandHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.ebrandName,
    };

    Meteor.call('brands.edit', this.state.brandBeingEdited, data, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Брэнд успешно изменен!', 'success', 'growl-top-right' );
        this.setState({
          ebrandName: '',
          brandBeingEdited: ''
        });
      }
    });
  }

  removeBrandHandler(e) {
    e.preventDefault();

    const currentButton = e.currentTarget,
          brandId = currentButton.getAttribute('data-id');

    Meteor.call('brands.remove', brandId, (err) => {
      if(err) {
        Bert.alert(err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert('Характеристика успешно удалена!', 'success', 'growl-top-right' );
      }
    })
  }

  createExtraFeatureHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.extraFeatureName
    };

    Meteor.call('extraFeatures.add', data, 'a', (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Характеристика успешно создана!', 'success', 'growl-top-right' );
        this.setState({
          extraFeatureName: '',
        });
      }
    });
  }

  updateExtraFeatureHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.eextraFeatureName,
    };

    Meteor.call('extraFeatures.edit', this.state.extraFeatureBeingEdited, data, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Характеристика успешно изменена!', 'success', 'growl-top-right' );
        this.setState({
          eextraFeatureName: '',
          extraFeatureBeingEdited: ''
        });
      }
    });
  }

  removeExtraFeatureHandler(e) {
    e.preventDefault();

    const currentButton = e.currentTarget,
          extraFeatureId = currentButton.getAttribute('data-id');

    Meteor.call('extraFeatures.remove', extraFeatureId, (err) => {
      if(err) {
        Bert.alert(err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert('Брэнд успешно удален!', 'success', 'growl-top-right' );
      }
    })
  }

  changePasswordHandler(e) {
    e.preventDefault();

    Accounts.changePassword(this.state.password, this.state.newPassword, (err) => {
      if(err) {
        Bert.alert( "Не верен старый пароль", 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Пароль успешно изменен!', 'success', 'growl-top-right' );
      }
    });
  }

  changePhotoHandler(e) { //when "change photo" button clicked
    e.preventDefault();

    qs('#input-photo').click();
  }

  changeFileHandler(e) { //when file is actually changed
    e.preventDefault();
  }

  dateSelectHandler(target, date) {
    moment.locale('ru');
    const formattedDate = moment(date).format('LL');
    target.value = formattedDate;

    this.setState({birthdate: Date.parse(date)});
  }

  logoutHandler(e) {
    e.preventDefault();
    Meteor.logout((err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'До свидания!', 'success', 'growl-top-right' );
        this.forceUpdate();
      }
    });
  }

  render() {
    if(this.props.user === undefined || !this.state.usersReady)
      return <div>загрузка ...</div>;

    const avatar = this.props.photos.filter((photo) => photo._id._str === this.props.user.profile.photo)[0],
          usersFiltered = this.props.users.filter((userItem) => userItem._id !== this.props.user._id);

    return (
      <SuperAdmin
        users={usersFiltered}
        user={this.props.user}

        editModalWillShow={this.editModalWillShow.bind(this)}
        updateUserHandler={this.updateUserHandler.bind(this)}
        createUserHandler={this.createUserHandler.bind(this)}
        removeUserHandler={this.removeUserHandler.bind(this)}

        editCategoryModalWillShow={this.editCategoryModalWillShow.bind(this)}
        updateCategoryHandler={this.updateCategoryHandler.bind(this)}
        createCategoryHandler={this.createCategoryHandler.bind(this)}
        removeCategoryHandler={this.removeCategoryHandler.bind(this)}

        editSubCategoryModalWillShow={this.editSubCategoryModalWillShow.bind(this)}
        updateSubCategoryHandler={this.updateSubCategoryHandler.bind(this)}
        createSubCategoryHandler={this.createSubCategoryHandler.bind(this)}
        removeSubCategoryHandler={this.removeSubCategoryHandler.bind(this)}

        editBrandModalWillShow={this.editBrandModalWillShow.bind(this)}
        updateBrandHandler={this.updateBrandHandler.bind(this)}
        createBrandHandler={this.createBrandHandler.bind(this)}
        removeBrandHandler={this.removeBrandHandler.bind(this)}

        editExtraFeatureModalWillShow={this.editExtraFeatureModalWillShow.bind(this)}
        updateExtraFeatureHandler={this.updateExtraFeatureHandler.bind(this)}
        createExtraFeatureHandler={this.createExtraFeatureHandler.bind(this)}
        removeExtraFeatureHandler={this.removeExtraFeatureHandler.bind(this)}

        changePasswordHandler={this.changePasswordHandler.bind(this)}
        changePhotoHandler={this.changePhotoHandler.bind(this)}
        changeFileHandler={this.changeFileHandler.bind(this)}
        dateSelectHandler={this.dateSelectHandler.bind(this)}
        logoutHandler={this.logoutHandler.bind(this)}
        userPhotos={userPhotos}
        avatar={avatar}
        context={this}
        cities={this.props.cities}
        sexes={this.props.sexes}

        categories={this.props.categories}
        subCategories={this.props.subCategories}
        brands={this.props.brands}
        extraFeatures={this.props.extraFeatures}
      />
    );
  }
}

export default createContainer(() => {
  return {
    user: Meteor.user(),
    photos: userPhotos.find().fetch(),
    users: UsersCollection.find().fetch(),
    cities: CitiesCollection.find().fetch(),
    sexes: SexesCollection.find().fetch(),

    categories: CategoriesCollection.find().fetch(),
    subCategories: SubCategoriesCollection.find().fetch(),
    brands: BrandsCollection.find().fetch(),
    extraFeatures: ExtraFeaturesCollection.find().fetch()
  }
}, SuperAdminContainer);
