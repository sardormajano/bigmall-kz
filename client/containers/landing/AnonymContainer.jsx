import React, {Component} from 'react';

export default class AnonymContainer extends Component {
    render() {
        return (
            <section className="mainContent clearfix notFound">
                <div className="container">
                <div className="row">
                    <div className="col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
                        <a href="index.html" className="navbar-brand pageLogo"><img src="img/logo.png" alt="logo" /></a>
                        <h2>Скоро откроемся!</h2>
                        <h3>Вебсайт находится на стадии разработки!</h3>
                    </div>
                </div>
                </div>
            </section>
        );
    }
}