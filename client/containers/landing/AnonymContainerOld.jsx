import React, {Component} from 'react';
import Anonym from '../../components/landing/Anonym';
import {createContainer} from 'meteor/react-meteor-data';

import {CategoriesCollection} from '../../../api/Categories';
import {SubCategoriesCollection} from '../../../api/SubCategories';
import {FullScreenSlidesCollection} from '../../../api/FullScreenSlides';
import {RecommendedCollectionsCollection} from '../../../api/RecommendedCollections';
import {RecommendedCategoriesCollection} from '../../../api/RecommendedCategories';

class AnonymContainer extends Component {
  componentWillMount() {
    Meteor.subscribe('SubCategories', () => this.setState({subCategoriesReady: true}));
    Meteor.subscribe('Categories', () => this.setState({categoriesReady: true}));
    Meteor.subscribe('FullScreenSlides', () => this.setState({fullScreenSlidesReady: true}));
    Meteor.subscribe('RecommendedCollections', () => this.setState({recommendedCollectionsReady: true}));
    Meteor.subscribe('RecommendedCategories', () => this.setState({recommendedCategoriesReady: true}));
  }

  constructor(props) {
    super(props);

    this.state = {
      searchText: '',
      searchCategory: '',

      subCategoriesReady: false,
      categoriesReady: false,
      fullScreenSlidesReady: false,
      recommendedCollectionsReady: false,
      recommendedCategoriesReady: false
    };
  }

  searchButtonHandler(e) {
    e.preventDefault();
    let {searchCategory, searchText} = this.state;

    searchCategory = searchCategory.length ? searchCategory : '-';
    searchText = searchText.length ? searchText: '-';

    window.location.href =
      `/merchandise-search/${searchCategory}/${searchText}`;
  }

  render() {
    if(this.props.user === undefined)
      return <div>загрузка...</div>;

    if(!this.state.subCategoriesReady || !this.props.subCategories.length) {
      return <div>загрузка...</div>;
    }

    if(!this.state.categoriesReady || !this.props.categories.length) {
      return <div>загрузка...</div>;
    }

    return (
      <Anonym
        context={this}
        user={this.props.user}
      />
    );
  }
}

export default createContainer(() => {
  return {
    user: Meteor.user(),
    categories: CategoriesCollection.find().fetch(),
    subCategories: SubCategoriesCollection.find().fetch(),
    fullScreenSlides: FullScreenSlidesCollection.find().fetch(),
    recommendedCategories: RecommendedCategoriesCollection.find().fetch(),
    recommendedCollections: RecommendedCollectionsCollection.find().fetch()
  };
}, AnonymContainer);
