import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import SuperAdmin from '../../components/landing/SuperAdmin';

import {userPhotos} from '../../../api/Users';
import {UsersCollection} from '../../../api/Users';
import {CategoriesCollection} from '../../../api/Categories';

import {FullScreenSlidesCollection, fullScreenSlidesPhotos} from '../../../api/FullScreenSlides';
import {RecommendedCollectionsCollection, recommendedCollectionsPhotos} from '../../../api/RecommendedCollections';
import {RecommendedCategoriesCollection, recommendedCategoriesPhotos} from '../../../api/RecommendedCategories';

class SuperAdminContainer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      // full screen slider
      fssTitle: '',
      fssSubtitle: '',
      fssText: '',
      fssPhoto: {},

      fssBeingEdited: '',

      efssTitle: '',
      efssSubtitle: '',
      efssText: '',
      efssPhoto: {},

      //recommended collections
      rclTitle: '',
      rclSubtitle: '',
      rclPhoto: {},

      rclBeingEdited: '',

      erclTitle: '',
      erclSubtitle: '',
      erclPhoto: {},

      //recommended categories
      rctCategory: '',
      rctPhoto: {},

      rctBeingEdited: '',

      erctTitle: '',
      erctSubtitle: '',
      erctPhoto: {},

      //subscriptions
      usersReady: false,
      fullScreenSlidesReady: false,
      fullScreenSlidesPhotosReady: false,
      recommendedCollectionsReady: false,
      recommendedCollectionsPhotosReady: false,
      recommendedCategoriesReady: false,
      recommendedCategoriesPhotosReady: false
    };
  }

  componentWillMount() {
    Meteor.subscribe('photos', (Meteor.userId()));

    Meteor.subscribe('Categories', {
      onReady: () => this.setState({categoriesReady: true})
    })

    Meteor.subscribe('UsersList', {
      onReady: () => {
        this.setState({usersReady: true});
      }
    });

    Meteor.subscribe('FullScreenSlides', {
      onReady: () => {
        this.setState({fullScreenSlidesReady: true});
      }
    });

    Meteor.subscribe('FullScreenSlidesPhotos', {
      onReady: () => {
        this.setState({fullScreenSlidesPhotosReady: true});
      }
    });

    Meteor.subscribe('RecommendedCollections', {
      onReady: () => {
        this.setState({recommendedCollectionsReady: true});
      }
    });

    Meteor.subscribe('RecommendedCollectionsPhotos', {
      onReady: () => {
        this.setState({recommendedCollectionsPhotosReady: true});
      }
    });

    Meteor.subscribe('RecommendedCategories', {
      onReady: () => {
        this.setState({recommendedCategoriesReady: true});
      }
    });

    Meteor.subscribe('RecommendedCategoriesPhotos', {
      onReady: () => {
        this.setState({recommendedCategoriesPhotosReady: true});
      }
    })
  }

  changePhotoHandler(e) { //when "change photo" button clicked
    e.preventDefault();

    qs('#input-photo').click();
  }

  changeFileHandler(e) { //when file is actually changed
    e.preventDefault();
  }

  //Full Screen Slider Event Handlers

  editFssModalWillShow(e) {
    const fssId = e.currentTarget.getAttribute('data-id'),
          currentFss = FullScreenSlidesCollection.findOne({_id: fssId});

    this.setState({
      efssTitle: currentFss.title,
      efssSubtitle: currentFss.subtitle,
      efssText: currentFss.text,
      efssPhoto: currentFss.photo,

      fssBeingEdited: fssId
    });
  }

  removeFssHandler(e) {
    e.preventDefault();

    const currentButton = e.currentTarget,
          fssId = currentButton.getAttribute('data-id');

    Meteor.call('fullScreenSlides.remove', fssId, {'deleted': true}, (err) => {
      if(err) {
        Bert.alert(err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert('Слайд успешно удален!', 'success', 'growl-top-right' );
      }
    })
  }

  createFssHandler(e) {
    const {fssTitle, fssSubtitle, fssText, fssPhoto} = this.state;

    Meteor.call('fullScreenSlides.add', {
      title: fssTitle,
      subtitle: fssSubtitle,
      text: fssText,
      photo: fssPhoto
    }, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Слайд успешно создан!', 'success', 'growl-top-right' );
        this.setState({
          fssTitle: '',
          fssSubtitle: '',
          fssText: '',
          fssPhoto: {}
        });
      }
    });
  }

  updateFssHandler(e) {
    const {efssTitle, efssSubtitle, efssText, efssPhoto, fssBeingEdited} = this.state;

    Meteor.call('fullScreenSlides.edit', fssBeingEdited,{
      title: efssTitle,
      subtitle: efssSubtitle,
      text: efssText,
    }, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Слайд успешно изменен!', 'success', 'growl-top-right' );
        this.setState({
          efssTitle: '',
          efssSubtitle: '',
          efssText: '',
          efssPhoto: {}
        });
      }
    });
  }

  //Recommended Collections Event Handlers

  editRclModalWillShow(e) {
    const rclId = e.currentTarget.getAttribute('data-id'),
          currentRcl = RecommendedCollectionsCollection.findOne({_id: rclId});

    this.setState({
      erclTitle: currentRcl.title,
      erclSubtitle: currentRcl.subtitle,
      erclPhoto: currentRcl.photo,

      rclBeingEdited: rclId
    });
  }

  removeRclHandler(e) {
    e.preventDefault();

    const currentButton = e.currentTarget,
          rclId = currentButton.getAttribute('data-id');

    Meteor.call('recommendedCollections.remove', rclId, {'deleted': true}, (err) => {
      if(err) {
        Bert.alert(err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert('Рекомендованная коллекция успешно удалена!', 'success', 'growl-top-right' );
      }
    })
  }

  createRclHandler(e) {
    const {rclTitle, rclSubtitle, rclText, rclPhoto} = this.state;

    Meteor.call('recommendedCollections.add', {
      title: rclTitle,
      subtitle: rclSubtitle,
      photo: rclPhoto
    }, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Рекомендованная коллекция успешно создана!', 'success', 'growl-top-right' );
        this.setState({
          rclTitle: '',
          rclSubtitle: '',
          rclPhoto: {}
        });
      }
    });
  }

  updateRclHandler(e) {
    const {erclTitle, erclSubtitle, erclPhoto, rclBeingEdited} = this.state;

    Meteor.call('recommendedCollections.edit', rclBeingEdited,{
      title: erclTitle,
      subtitle: erclSubtitle,
    }, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Рекомендованная коллекция успешно изменена!', 'success', 'growl-top-right' );
        this.setState({
          erclTitle: '',
          erclSubtitle: '',
          erclPhoto: {}
        });
      }
    });
  }

  //Recommended Categories Event Handlers

  editRctModalWillShow(e) {
    const rctId = e.currentTarget.getAttribute('data-id'),
          currentRct = RecommendedCategoriesCollection.findOne({_id: rctId});

    this.setState({
      erctCategory: currentRct.category,
      erctPhoto: currentRct.photo,

      rctBeingEdited: rctId
    });
  }

  removeRctHandler(e) {
    e.preventDefault();

    const currentButton = e.currentTarget,
          rctId = currentButton.getAttribute('data-id');

    Meteor.call('recommendedCategories.remove', rctId, {'deleted': true}, (err) => {
      if(err) {
        Bert.alert(err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert('Рекомендованная категория успешно удалена!', 'success', 'growl-top-right' );
      }
    })
  }

  createRctHandler(e) {
    const {rctCategory, rctPhoto} = this.state;

    Meteor.call('recommendedCategories.add', {
      category: rctCategory,
      photo: rctPhoto
    }, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Фотография для категории успешно добавлена!', 'success', 'growl-top-right' );
        this.setState({
          rctCategory: '',
          rctPhoto: '',
        });
      }
    });
  }

  updateRctHandler(e) {
    const {erctCategory, erctPhoto, rctBeingEdited} = this.state;

    Meteor.call('recommendedCategories.edit', rctBeingEdited, {
      category: erctCategory
    }, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Рекомендованная категория успешно изменена!', 'success', 'growl-top-right' );
        this.setState({
          erctTitle: '',
          erctSubtitle: '',
          erctPhoto: {}
        });
      }
    });
  }

  render() {
    if(this.props.user === undefined || !this.state.usersReady)
      return <div>загрузка ...</div>;

    if(this.props.user === null && this.props.user !== undefined) {
      window.location.href = 'signup-login';
    }

    const avatar = this.props.photos.filter((photo) => photo._id._str === this.props.user.profile.photo)[0];

    return (
      <SuperAdmin
        user={this.props.user}
        avatar={avatar}
        context={this}
        changePhotoHandler={this.changePhotoHandler}
        fssPhotos={fullScreenSlidesPhotos}
        rclPhotos={recommendedCollectionsPhotos}
        rctPhotos={recommendedCategoriesPhotos}
      />
    );
  }
}

export default createContainer(() => {
  return {
    user: Meteor.user(),
    photos: userPhotos.find().fetch(),

    categories: CategoriesCollection.find().fetch(),
    fss: FullScreenSlidesCollection.find().fetch(),
    rcl: RecommendedCollectionsCollection.find().fetch(),
    rct: RecommendedCategoriesCollection.find().fetch(),
  }
}, SuperAdminContainer);
