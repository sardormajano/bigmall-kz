import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Anonym from '../../components/merchandise-search/Anonym';

import {MerchandiseCollection, merchandisePhotos} from '../../../api/Merchandise';
import {SubCategoriesCollection} from '../../../api/SubCategories';

class AnonymContainer extends Component {
  componentWillMount() {
    Meteor.subscribe('MerchandisePhotos');
    Meteor.subscribe('AllMerchandise');
    Meteor.subscribe('SubCategories', () => this.setState({subCategoriesReady: true}));
  }

  constructor(props) {
    super(props);

    const {searchText, searchCategory} = this.props.params;

    this.state = {
      searchText: searchText || '',
      searchCategory: searchCategory || '',

      subCategoriesReady: false
    }
  }

  searchButtonHandler(e) {
    e.preventDefault();
  }

  render() {
    if(!this.state.subCategoriesReady || !this.props.subCategories.length) {
      return <div>загрузка...</div>;
    }

    const {merchandise} = this.props,
          {searchText, searchCategory} = this.state,
          merchandiseFiltered = merchandise.filter((item) => {
            if(['', '-'].indexOf(searchText) > -1 && ['0', '-'].indexOf(searchCategory) > -1)
              return true;

            if(item.subCategory === searchCategory || ['0', '-'].indexOf(searchCategory) > -1) {
              if(['', '-'].indexOf(searchText) > -1
                || item.name.toLowerCase().indexOf(searchText.toLowerCase()) > -1)
                return true;
            }

            return false;
          });

    return (
      <Anonym
        context={this}
        merchandiseFiltered={merchandiseFiltered}
      />
    );
  }
}

export default createContainer(() => {
  return {
    user: Meteor.user(),
    merchandise: MerchandiseCollection.find().fetch(),
    merchandisePhotos: merchandisePhotos.find().fetch(),
    subCategories: SubCategoriesCollection.find().fetch()
  }
}, AnonymContainer);
