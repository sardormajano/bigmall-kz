import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

import SalesAssistant from '../../components/merchandise/SalesAssistant';
import {userPhotos} from '../../../api/Users';
import {merchandisePhotos} from '../../../api/Merchandise';
import {UsersCollection} from '../../../api/Users';

import {CategoriesCollection} from '../../../api/Categories';
import {SubCategoriesCollection} from '../../../api/SubCategories';
import {BrandsCollection} from '../../../api/Brands';
import {MerchandiseCollection} from '../../../api/Merchandise';
import {StoresCollection} from '../../../api/Stores';

class SalesAssistantContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      usersReady: false,
      categoriesReady: false,
      subCategoriesReady: false,
      brandsReady: false,
      merchandiseReady: false,
      storesReady: false,

      name: '',
      category: '',
      subCategory: '',
      brand: '',
      article: '',
      extraFeatures: '',
      store: '',
      numInStore: '',
      price: '',
      discount: '',
      photos: [],
    }
  }

  componentWillMount() {
    Meteor.subscribe('photos', (Meteor.userId()));
    Meteor.subscribe('MerchandisePhotos', {
      onReady: () => {

      }
    });

    Meteor.subscribe('UsersList', {
      onReady: () => {
        this.setState({usersReady: true});
      }
    });

    Meteor.subscribe('CurrentUser', {
      onReady: () => {
        if(!this.props.user) {
          browserHistory.push('signup-login');
          return ;
        }
      }
    });

    Meteor.subscribe('Categories', {
      onReady: () => {
        this.setState({
          categoriesReady: true
        })
      }
    });

    Meteor.subscribe('SubCategories', {
      onReady: () => {
        this.setState({
          subCategoriesReady: true
        })
      }
    });

    Meteor.subscribe('Brands', {
      onReady: () => {
        this.setState({
          brandsReady: true
        })
      }
    });

    Meteor.subscribe('Merchandise', {
      onReady: () => {
        this.setState({
          merchandiseReady: true
        });
      }
    });

    Meteor.subscribe('Stores', {
      onReady: () => {
        this.setState({
          storesReady: true
        });
      }
    });
  }

  logoutHandler(e) {
    e.preventDefault();
    Meteor.logout((err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'До свидания!', 'success', 'growl-top-right' );
      }
    });
  }

  changePhotoHandler(e) { //when "change photo" button clicked
    e.preventDefault();

    qs('#input-photo').click();
  }

  removePhotoHandler(e) {
    e.preventDefault();

    let photos = this.state.photos,
          fileName = e.currentTarget.getAttribute('data-file-name');

    photos = photos.filter((photo) => photo.fileName !== fileName);

    this.setState({photos});
  }

  changeFileHandler(e) { //when file is actually changed
    e.preventDefault();
  }

  createMerchandiseHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.name,
      category: this.state.category,
      subCategory: this.state.subCategory,
      brand: this.state.brand,
      article: this.state.article,
      extraFeatures: this.state.extraFeatures,
      store: this.state.store,
      numInStore: this.state.numInStore,
      price: this.state.price,
      discount: this.state.discount,
      photos: this.state.photos
    };

    Meteor.call('merchandise.add', data, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Товар успешно создан!', 'success', 'growl-top-right' );

        this.setState({
          name: '',
          category: '',
          subCategory: '',
          brand: '',
          article: '',
          extraFeatures: '',
          store: '',
          numInStore: '',
          price: '',
          discount: '',
          photos: [],
        });
      }
    });
  }

  removeMerchandiseHandler(e) {
    e.preventDefault();

    const _id = e.currentTarget.getAttribute('data-id');

    Meteor.call('merchandise.remove', _id, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Товар успешно удален!', 'success', 'growl-top-right' );
      }
    });
  }

  componentWillUpdate(nextProps) {
    if(nextProps.user === null) {
      browserHistory.push('signup-login');
    }
  }

  render() {
    if(!this.props.user || !this.state.usersReady)
      return <div>загрузка ...</div>;

    const avatar = this.props.photos.filter((photo) => photo._id._str === this.props.user.profile.photo)[0],
          subCategoriesFiltered = this.props.subCategories.filter((subCategory) => subCategory.category === this.state.category);

    return (
      <SalesAssistant
        user={this.props.user}
        logoutHandler={this.logoutHandler.bind(this)}
        userPhotos={userPhotos}
        merchandisePhotos={merchandisePhotos}
        avatar={avatar}

        context={this}

        categories={this.props.categories}
        subCategories={subCategoriesFiltered}
        brands={this.props.brands}
        merchandise={this.props.merchandise}
        stores={this.props.stores}

        removeMerchandiseHandler={this.removeMerchandiseHandler}

        removePhotoHandler={this.removePhotoHandler.bind(this)}

        changePhotoHandler={this.changePhotoHandler.bind(this)}
        createMerchandiseHandler={this.createMerchandiseHandler.bind(this)}
      />
    );
  }
}

export default createContainer(() => {
  return {
    user: Meteor.user(),
    photos: userPhotos.find().fetch(),
    merchandisePhotos: merchandisePhotos.find().fetch(),

    categories: CategoriesCollection.find().fetch(),
    subCategories: SubCategoriesCollection.find().fetch(),
    brands: BrandsCollection.find().fetch(),
    merchandise: MerchandiseCollection.find().fetch(),
    stores: StoresCollection.find().fetch()
  }
}, SalesAssistantContainer);
