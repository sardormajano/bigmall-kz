import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import {createContainer} from 'meteor/react-meteor-data';

import {qs} from '../../lib/coreLib';
import moment from '../../lib/moment-with-locales.min.js';

import SuperAdmin from '../../components/profiles/SuperAdmin';
import {userPhotos} from '../../../api/Users';
import {UsersCollection} from '../../../api/Users';
import {CitiesCollection} from '../../../api/Cities';
import {SexesCollection} from '../../../api/Sexes';

class SuperAdminContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      phone: '',
      email: '',
      password: '',
      newPassword: '',
      city: '',
      birthdate: '',
      sex: '',

      citiesReady: false,
      sexesReady: false
    };
  }

  componentWillMount() {
    Meteor.subscribe('photos', (Meteor.userId()));

    Meteor.subscribe('Cities', {
      onReady: () => {
        this.setState({
          citiesReady: true
        })
      }
    });

    Meteor.subscribe('Sexes', {
      onReady: () => {
        this.setState({
          sexesReady: true
        })
      }
    });

    Meteor.subscribe('CurrentUser', {
      onReady: () => {
        const {user} = this.props;

        this.setState({
          firstName: user.profile.firstName || ''  ,
          lastName: user.profile.lastName || '',
          phone: user.profile.phone || '',
          email: user.emails[0].address || '',
          city: user.profile.city || '',
          birthdate: user.profile.birthdate || '',
          sex: user.profile.sex || ''
        });
      }
    });
  }

  changePasswordHandler(e) {
    e.preventDefault();

    Accounts.changePassword(this.state.password, this.state.newPassword, (err) => {
      if(err) {
        Bert.alert( "Не верен старый пароль", 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Пароль успешно изменен!', 'success', 'growl-top-right' );
      }
    });
  }

  dateSelectHandler(target, date) {
    moment.locale('ru');
    const formattedDate = moment(date).format('LL');
    target.value = formattedDate;

    this.setState({birthdate: Date.parse(date)});
  }

  saveChangesHandler(e) {
    e.preventDefault();

    Meteor.call('user.update', {
      'profile.firstName': this.state.firstName,
      'profile.lastName': this.state.lastName,
      'profile.city': this.state.city,
      'profile.phone': this.state.phone,
      'profile.birthdate': this.state.birthdate,
      'profile.sex': this.state.sex,
      'emails': [{address: this.state.email, verified: false}],
    }, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Данные пользователя успешно изменены!', 'success', 'growl-top-right' );
      }
    });
  }

  changePhotoHandler(e) { //when "change photo" button clicked
    e.preventDefault();

    qs('#input-photo').click();
  }

  changeFileHandler(e) { //when file is actually changed
    e.preventDefault();
  }

  logoutHandler(e) {
    e.preventDefault();
    Meteor.logout((err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        window.location.reload();
      }
    });
  }

  componentWillUpdate(nextProps) {
    if(nextProps.user === null) {
      browserHistory.push('signup-login');
    }
  }

  render() {

    if(!this.props.user || !this.state.citiesReady)
      return <div>загрузка ...</div>;

    const avatar = this.props.photos.filter((photo) => photo._id._str === this.props.user.profile.photo)[0];

    return (
      <SuperAdmin
        user={this.props.user}
        changePasswordHandler={this.changePasswordHandler.bind(this)}
        saveChangesHandler={this.saveChangesHandler.bind(this)}
        changePhotoHandler={this.changePhotoHandler.bind(this)}
        changeFileHandler={this.changeFileHandler.bind(this)}
        logoutHandler={this.logoutHandler.bind(this)}
        dateSelectHandler={this.dateSelectHandler.bind(this)}
        userPhotos={userPhotos}
        avatar={avatar}
        context={this}
        cities={this.props.cities}
        sexes={this.props.sexes}
      />
    );
  }
}

export default createContainer(() => {
  return {
    photos: userPhotos.find().fetch(),

    user: Meteor.user(),
    cities: CitiesCollection.find().fetch(),
    sexes: SexesCollection.find().fetch()
  }
}, SuperAdminContainer);
