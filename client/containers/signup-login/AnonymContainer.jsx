import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

import Anonym from '../../components/signup-login/Anonym';

class AnonymContainer extends Component {
  constructor() {
    super();

    this.state = {
      role: 'c',
      storeName: '',
      suPassword: '',
      siUsername: '',
      siPassword: '',
      email: '',
      firstName: '',
      lastName: '',
      agree: false
    };
  }

  resetState() {
    this.setState({
      role: 'c',
      suUsername: '',
      suPassword: '',
      siUsername: '',
      siPassword: '',
      email: '',
      firstName: '',
      lastName: '',
      storeName: '',
      agree: false
    });
  }

  logoutHandler(e) {
    e.preventDefault();
    Meteor.logout((err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'До свидания!', 'success', 'growl-top-right' );
      }
    });
  }

  signUpHandler(e) {
    e.preventDefault();

    const userOptions = {
      username: this.state.email,
      password: this.state.suPassword,
      email: this.state.email,
      profile: {
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        storeName: this.state.storeName
      }
    };

    Meteor.call('user.create', userOptions, this.state.role, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Акаунт успешно создан!', 'success', 'growl-top-right' );
        this.resetState();
      }
    });
  }

  signInHandler(e) {
    e.preventDefault();

    const username = this.state.siUsername,
          password = this.state.siPassword;

    Meteor.loginWithPassword(username, password, (err) => {
      if(err) {
        Bert.alert( 'Неправильный логин или пароль!', 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Добро пожаловать в BigMall!', 'success', 'growl-top-right' );
        browserHistory.push('profile');
      }
    });
  }

  componentWillUpdate(nextProps) {
    if(nextProps.user !== null && nextProps.user !== undefined ) {
      window.location.href = 'profile';
    }
  }

  render() {
    if(this.props.user === undefined)
      return <div>загрузка...</div>

    if(this.props.user !== null && this.props.user !== undefined) {
      window.location.href = 'profile';
    }

    return (
      <Anonym
        user={this.props.user}
        signUpHandler={this.signUpHandler.bind(this)}
        signInHandler={this.signInHandler.bind(this)}
        logoutHandler={this.logoutHandler.bind(this)}
        context={this}/>
    );
  }
}

export default createContainer(()=> {
  return {
    user: Meteor.user()
  };
}, AnonymContainer);
