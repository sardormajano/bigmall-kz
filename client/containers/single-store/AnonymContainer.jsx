import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Anonym from '../../components/single-store/Anonym.jsx';

import {StoresCollection} from '../../../api/Stores';
import {CategoriesCollection} from '../../../api/Categories';
import {SubCategoriesCollection} from '../../../api/SubCategories';
import {MerchandiseCollection, merchandisePhotos} from '../../../api/Merchandise';

class AnonymContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      storesReady: false,
      categoriesReady: false,
      subCategoriesReady: false,
      merchandiseReady: false,
      merchandisePhotosReady: false,

      currentMerchandiseItem: undefined
    }
  }

  componentWillMount() {
    Meteor.subscribe('Stores', {
      onReady: () => {
        this.setState({storesReady: true});
      }
    });

    Meteor.subscribe('Categories', {
      onReady: () => {
        this.setState({categoriesReady: true});
      }
    });

    Meteor.subscribe('SubCategories', {
      onReady: () => {
        this.setState({subCategoriesReady: true});
      }
    });

    Meteor.subscribe('StoreMerchandise', this.props.params.storeId, {
      onReady: () => {
        this.setState({merchandiseReady: true});
      }
    });

    Meteor.subscribe('MerchandisePhotos', {
      onReady: () => {
        this.setState({merchandisePhotosReady: true})
      }
    })
  }

  subscribeHandler(e) {
    e.preventDefault();

    Meteor.call('user.subscribe', this.props.params.storeId, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Вы подписались на магазин!', 'success', 'growl-top-right' );
      }
    });
  }

  unsubscribeHandler(e) {
    e.preventDefault();
    
    Meteor.call('user.unsubscribe', this.props.params.storeId, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Вы отписались от магазина!', 'success', 'growl-top-right' );
      }
    });
  }

  render() {
    const {stores, categories, subCategories, merchandise, merchandisePhotosArray} = this.props;

    const currentStore = stores.filter((store) => store._id === this.props.params.storeId)[0];

    if(!this.state.storesReady || !this.state.categoriesReady
          || !this.state.subCategoriesReady || !this.state.merchandiseReady
          || !this.state.merchandisePhotosReady || !currentStore)
      return <div>Загрузка...</div>;

    return (
      <Anonym
        context={this}
        currentStore={currentStore}
        categories={categories}
        subCategories={subCategories}
        merchandise={merchandise}
        merchandisePhotos={merchandisePhotosArray}
      />
    );
  }
}

export default createContainer(() => {
  return {
    user: Meteor.user(),
    stores: StoresCollection.find().fetch(),
    categories: CategoriesCollection.find().fetch(),
    subCategories: SubCategoriesCollection.find().fetch(),
    merchandise: MerchandiseCollection.find().fetch(),
    merchandisePhotosArray: merchandisePhotos.find().fetch()
  }
}, AnonymContainer);
