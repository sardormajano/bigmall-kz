import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import {browserHistory} from 'react-router';

import SalesAssistant from '../../components/stores/SalesAssistant';

import {userPhotos} from '../../../api/Users';
import {StoresCollection} from '../../../api/Stores';
import {storeLogos} from '../../../api/Stores';

class SalesAssistantContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      storesReady: false,
      userReady: false,

      storeName: '',
      logo: {},
      address: '',
      hasDelivery: false,

      estoreName: '',
      elogo: {},
      eaddress: '',
      ehasDelivery: false,

      storeBeingEdited: ''
    }
  }

  componentWillMount() {
    Meteor.subscribe('photos', (Meteor.userId()));

    Meteor.subscribe('StoreLogos', (Meteor.userId()));

    Meteor.subscribe('Stores', {
      onReady: () => {
        this.setState({storesReady: true});
      }
    });

    Meteor.subscribe('CurrentUser', {
      onReady: () => {
        if(!this.props.user) {
          browserHistory.push('signup-login');
          return ;
        }

        this.setState({userReady: true});
      }
    });
  }

  changePhotoHandler(e) { //when "change photo" button clicked
    e.preventDefault();

    qs('#input-photo').click();
  }

  editStoreModalWillShow(e) {
    const storeId = e.currentTarget.getAttribute('data-id'),
          currentStore = StoresCollection.findOne({_id: storeId});

    this.setState({
      estoreName: currentStore.name,
      elogo: currentStore.logo,
      eaddress: currentStore.address,
      ehasDelivery: currentStore.hasDelivery,
      storeBeingEdited: currentStore._id
    });
  }

  createStoreHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.storeName,
      logo: this.state.logo,
      address: this.state.address,
      hasDelivery: this.state.hasDelivery
    };

    console.log(data);

    Meteor.call('stores.add', data, 'a', (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Магазин успешно создан!', 'success', 'growl-top-right' );
        this.setState({
          storeName: '',
          logo: {},
          address: ''
        });
      }
    });
  }

  updateStoreHandler(e) {
    e.preventDefault();

    const data = {
      name: this.state.estoreName,
      address: this.state.eaddress,
      logo: this.state.elogo,
      hasDelivery: this.state.ehasDelivery
    };

    Meteor.call('stores.edit', this.state.storeBeingEdited, data, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'Магазин успешно изменен!', 'success', 'growl-top-right' );
        this.setState({
          estoreName: '',
          storeBeingEdited: ''
        });
      }
    });
  }

  removeStoreHandler(e) {
    e.preventDefault();

    const currentButton = e.currentTarget,
          storeId = currentButton.getAttribute('data-id');

    Meteor.call('stores.remove', storeId, (err) => {
      if(err) {
        Bert.alert(err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert('Магазин успешно удален!', 'success', 'growl-top-right' );
      }
    })
  }

  render() {
    if(this.props.user === undefined || !this.state.userReady)
      return <div>загрузка ...</div>;

    if(this.props.user === null && this.props.user !== undefined) {
      window.location.href = 'signup-login';
    }

    const avatar = this.props.photos.filter((photo) => photo._id._str === this.props.user.profile.photo)[0];

    return (
      <SalesAssistant
        avatar={avatar}
        changePhotoHandler={this.changePhotoHandler.bind(this)}
        userPhotos={userPhotos}
        storeLogos={storeLogos}
        stores={this.props.stores}

        editStoreModalWillShow={this.editStoreModalWillShow.bind(this)}

        removeStoreHandler={this.removeStoreHandler.bind(this)}
        updateStoreHandler={this.updateStoreHandler.bind(this)}
        createStoreHandler={this.createStoreHandler.bind(this)}

        context={this}
      />
    );
  }
}

export default createContainer(() => {
  return {
    photos: userPhotos.find().fetch(),
    user: Meteor.user(),
    stores: StoresCollection.find().fetch(),
  }
}, SalesAssistantContainer);
