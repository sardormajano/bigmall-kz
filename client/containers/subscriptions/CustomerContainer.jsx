import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Customer from '../../components/subscriptions/Customer';

import {userPhotos} from '../../../api/Users';
import {StoresCollection} from '../../../api/Stores';
import {storeLogos} from '../../../api/Stores';

class CustomerContainer extends Component {
  componentWillMount() {
    Meteor.subscribe('photos', (Meteor.userId()));

    Meteor.subscribe('StoreLogos', (Meteor.userId()));

    Meteor.subscribe('Subscriptions', {
      onReady: () => {
        this.setState({storesReady: true});
      }
    });
  }

  changePhotoHandler(e) { //when "change photo" button clicked
    e.preventDefault();

    qs('#input-photo').click();
  }

  render() {
    if(this.props.user === undefined)
      return <div>загрузка ...</div>;

    const avatar = this.props.photos.filter((photo) => photo._id._str === this.props.user.profile.photo)[0];

    return (
      <Customer
        avatar={avatar}
        changePhotoHandler={this.changePhotoHandler.bind(this)}
        userPhotos={userPhotos}
        storeLogos={storeLogos}
        stores={this.props.stores}
      />
    );
  }
}

export default createContainer(() => {
  return {
    photos: userPhotos.find().fetch(),
    user: Meteor.user(),
    stores: StoresCollection.find().fetch(),
  }
}, CustomerContainer);
