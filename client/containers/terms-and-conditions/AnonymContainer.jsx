import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Anonym from '../../components/terms-and-conditions/Anonym';

class AnonymContainer extends Component {
  logoutHandler(e) {
    e.preventDefault();
    Meteor.logout((err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'growl-top-right' );
      }
      else {
        Bert.alert( 'До свидания!', 'success', 'growl-top-right' );
      }
    });
  }

  render() {
    if(this.props.user === undefined)
      return <div>загрузка...</div>

    return (
      <Anonym
        logoutHandler={this.logoutHandler.bind(this)}
        user={this.props.user}/>
    );
  }
}

export default createContainer(() => {
  return {
    user: Meteor.user()
  };
}, AnonymContainer)
