import React from 'react';

import App from './components/App';

import SignUpLogin from './containers/signup-login/AnonymContainer';
import TermsAndConditions from './containers/terms-and-conditions/AnonymContainer';

//LANDING PAGE
import LandingAnonym from './containers/landing/AnonymContainer';
import LandingSuperAdmin from './containers/landing/SuperAdminContainer';

//PROFILE PAGES
import ProfileSuperAdmin from './containers/profiles/SuperAdminContainer';

//DICTIONARY PAGES
import DictionarySuperAdmin from './containers/dictionaries/SuperAdminContainer';

//MERCHANDISE PAGES
import MerchandiseSalesAssistant from './containers/merchandise/SalesAssistantContainer';
import StoresSalesAssistant from './containers/stores/SalesAssistantContainer';
import SingleStoreAnonym from './containers/single-store/AnonymContainer';
import MerchandiseSearch from './containers/merchandise-search/AnonymContainer';

//SUBSCRIPTIONS PAGES
import SubscriptionsCustomer from './containers/subscriptions/CustomerContainer';

import {Router, Route, IndexRoute, browserHistory} from 'react-router';

export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={LandingAnonym} />
      <Route path="landing-super-admin" component={LandingSuperAdmin} />
      <Route path="signup-login" component={SignUpLogin} />
      <Route path="terms-and-conditions" component={TermsAndConditions} />
      <Route path="profile" component={ProfileSuperAdmin} />
      <Route path="profile-super-admin" component={ProfileSuperAdmin} />
      <Route path="dictionary-super-admin" component={DictionarySuperAdmin} />
      <Route path="merchandise-sales-assistant" component={MerchandiseSalesAssistant} />
      <Route path="stores-sales-assistant" component={StoresSalesAssistant} />
      <Route path="single-store/:storeId" component={SingleStoreAnonym} />
      <Route path="merchandise-search(/:searchCategory)(/:searchText)" component={MerchandiseSearch} />
      <Route path="subscriptions-customer" component={SubscriptionsCustomer} />
    </Route>
  </Router>
);
