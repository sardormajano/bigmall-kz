import { Meteor } from 'meteor/meteor';

import {CitiesCollection} from '../api/Cities';
import {SexesCollection} from '../api/Sexes';
import {UsersCollection} from '../api/Users';

Meteor.startup(() => {
  const cities = [
     {
        "name": "Астана"
     },
     {
        "name": "Атырау"
     },
     {
        "name": "Есик"
     },
     {
        "name": "Казалинск"
     },
     {
        "name": "Капчагай"
     },
     {
        "name": "Лисаковск"
     },
     {
        "name": "Степногорск"
     },
     {
        "name": "Степняк"
     },
     {
        "name": "Ушарал"
     },
     {
        "name": "Хромтау"
     },
     {
        "name": "Абай"
     },
     {
        "name": "Алга"
     },
     {
        "name": "Алматы"
     },
     {
        "name": "Атбасар"
     },
     {
        "name": "Балхаш"
     },
     {
        "name": "Жетысай"
     },
     {
        "name": "Зыряновск"
     },
     {
        "name": "Кандыагаш"
     },
     {
        "name": "Костанай"
     },
     {
        "name": "Павлодар"
     },
     {
        "name": "Петропавловск"
     },
     {
        "name": "Приозерск"
     },
     {
        "name": "Риддер"
     },
     {
        "name": "Сары-Агаш"
     },
     {
        "name": "Серебрянск"
     },
     {
        "name": "Талгар"
     },
     {
        "name": "Тараз"
     },
     {
        "name": "Текели"
     },
     {
        "name": "Темиртау"
     },
     {
        "name": "Форт-Шевченко"
     },
     {
        "name": "Аксай"
     },
     {
        "name": "Актюбинск"
     },
     {
        "name": "Аральск "
     },
     {
        "name": "Аркалык"
     },
     {
        "name": "Асу-Булак"
     },
     {
        "name": "Байконур"
     },
     {
        "name": "Жанаозен"
     },
     {
        "name": " Жем"
     },
     {
        "name": "Житикара"
     },
     {
        "name": "Каратау"
     },
     {
        "name": "Кентау"
     },
     {
        "name": "Кульсары"
     },
     {
        "name": "Курчатов"
     },
     {
        "name": "Кызылорда"
     },
     {
        "name": "Макинск"
     },
     {
        "name": "Мамлютка"
     },
     {
        "name": "Рудный"
     },
     {
        "name": "Сергеевка"
     },
     {
        "name": "Уральск"
     },
     {
        "name": "Усть-Каменогорск"
     },
     {
        "name": "Достык"
     },
     {
        "name": "Шымкент"
     },
     {
        "name": " Акколь"
     },
     {
        "name": "Актау"
     },
     {
        "name": "Аягоз"
     },
     {
        "name": "Булаево"
     },
     {
        "name": "Державинск"
     },
     {
        "name": "Есиль"
     },
     {
        "name": "Жанатас"
     },
     {
        "name": "Жаркент"
     },
     {
        "name": "Зайсан"
     },
     {
        "name": "Караганда"
     },
     {
        "name": "Каражал"
     },
     {
        "name": "Каскелен"
     },
     {
        "name": "Кокшетау"
     },
     {
        "name": "Сарканд"
     },
     {
        "name": "Сатпаев"
     },
     {
        "name": "Семипалатинск"
     },
     {
        "name": "Тайынша"
     },
     {
        "name": "Талдыкорган"
     },
     {
        "name": "Темир"
     },
     {
        "name": "Туркестан"
     },
     {
        "name": "Уштобе"
     },
     {
        "name": "Аксу"
     },
     {
        "name": "Ерейментау"
     },
     {
        "name": "Жезказган"
     },
     {
        "name": "Каркаралинск"
     },
     {
        "name": "Ленгер"
     },
     {
        "name": "Кордай"
     },
     {
        "name": "Отеген Батыра"
     }
  ];

  if(!CitiesCollection.find({}).count()) {
    cities.forEach((city) => {
      CitiesCollection.insert(city);
    });
  }

  const sexes = [
    { _id: '0', name: 'жен'},
    { _id: '1', name: 'муж'}
  ];

  if(!SexesCollection.find({}).count()) {
    sexes.forEach((sex) => {
      SexesCollection.insert(sex);
    });
  }

  if(!UsersCollection.findOne({username: 'superadmin'})) {
    const superadminOptions = {
            username: 'superadmin',
            password: 'superadmin',
            email: 'superadmin',
            profile: {
              firstName: 'Администратор',
              lastName: 'Супер'
            }
          },
          customerOptions = {
            username: 'customer',
            password: 'customer',
            email: 'customer',
            profile: {
              firstName: 'Клиент',
              lastName: 'Покупатель'
            }
          },
          salesAssistantOptions = {
            username: 'salesAssistant',
            password: 'salesAssistant',
            email: 'salesAssistant',
            profile: {
              firstName: 'Продавец',
              lastName: 'Товаров'
            }
          };

    Meteor.call('user.create', superadminOptions, 'a');
    Meteor.call('user.create', customerOptions, 'c');
    Meteor.call('user.create', salesAssistantOptions, 'sa');
  }

  //a - admin, sa - sales assistant, c - client/customer
});
